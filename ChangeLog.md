# ChangeLog
## Unreleased

## v0.4.0

- Parser works (even if without token types)
- Temporary: rxcc can parse files only

## v0.3.0

- Another rewrite from scratch
- Lexer works
- First error output (ever!)

## v0.2.2

- Complete rewrite from scratch (resulting in a different interface)
- Remove unnecessary bonus features (makes development faster)

## v0.1.0

- First release!
- Add possibility to define custom file reading methods
- Add the lexer pipeline stage, managing memory, buffer and file positions
- Implement the default lexer, handling comments and usual word separation
- Allow extending the default extension, currently only consisting out of a lexer, to have new word types
