/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "path.hpp"

#include "utils/config.hpp"

#ifdef HAVE_FILESYSTEM
#	include <filesystem>
#endif
#ifdef HAVE_STRING
#	include <string>
#endif

namespace utils::format {
	std::string path(const std::filesystem::path& p) {
		if(!p.is_absolute()) {
			return p.lexically_normal();
		}
		const std::filesystem::path relative = p.lexically_relative(std::filesystem::current_path()).lexically_normal();
		if(*relative.begin() == "..") {
			return p.lexically_normal();
		} else {
			return relative;
		}
	}
}
