/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_TRAVERSE_PREPEND_HPP
#define UTILS_TRAVERSE_PREPEND_HPP

/*!
 * @brief Prepend to traversal list.
 *
 * When one has the first element for tree traversal, this can be used to return it:
 *
 * 	const Error* Child::traverseError(std::size_t& number) const {
 * 		UTILS_TRAVERSE_PREPEND(this->error1, number);
 * 		UTILS_TRAVERSE_PREPEND(this->error2, number);
 * 		return nullptr;
 * 	}
 */
#define UTILS_TRAVERSE_PREPEND(value, number) \
	if(auto result = value) { \
		if(number == 0) { \
			return result; \
		} else { \
			--number; \
		} \
	}

#endif
