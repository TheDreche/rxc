/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_TRAVERSE_GETITEM_HPP
#define UTILS_TRAVERSE_GETITEM_HPP

/*!
 * @brief Traverse through child.
 *
 * When one wants to traverse multiple children of a parent node, this can be used to traverse one of them:
 *
 * 	const Error* Child::traverseError(std::size_t& number) const {
 * 		UTILS_TRAVERSE_GETITEM(this->firstChild->traverseError(number));
 * 		UTILS_TRAVERSE_GETITEM(this->secondChild->traverseError(number));
 * 		return nullptr;
 * 	}
 */
#define UTILS_TRAVERSE_GETITEM(value) \
	if(auto result = value) { \
		return result; \
	}

#endif
