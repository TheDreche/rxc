/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_VARIANT_UPCAST_HPP
#define UTILS_VARIANT_UPCAST_HPP

#include "config.hpp"
#ifdef HAVE_TYPE_TRAITS
#	include <type_traits>
#endif
#ifdef HAVE_VARIANT
#	include <variant>
#endif

namespace utils {
	template<class Superclass, class VariantT>
	class VariantUpcast final {
	public:
		Superclass* value;

	public:
		VariantUpcast(const VariantT& variant) {
			std::visit(*this, variant);
		}

	public:
		template<
			class VariantMemberT,
			typename std::enable_if<
				!std::is_convertible<
					VariantMemberT*,
					Superclass
				>::value,
				char
			>::type = '\0'
		>
		void operator()(VariantMemberT&) {
			this->value = nullptr;
		}

		template<
			class VariantMemberT,
			typename std::enable_if<
				std::is_convertible<
					VariantMemberT*,
					Superclass
				>::value,
				bool
			>::type = true
		>
		void operator()(VariantMemberT& member) {
			this->value = &member;
		}
	};
}

#endif
