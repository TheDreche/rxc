/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of rxcc.
 *
 * rxcc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rxcc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rxcc.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * @file A wrapper file around gettext.h.
 * 
 * It defines macros.
 */

#ifndef GETTEXT_HPP
#define GETTEXT_HPP

#define DEFAULT_TEXT_DOMAIN "rxc"

#include "gettext.h"

#define _(Msgid) gettext(Msgid)
#define _n(Msgid1, Msgid2, N) ngettext(Msgid1, Msgid2, N)
#define _noop(Msgid) Msgid

#include <string>

#include "rxc/localized_string.hpp"

namespace utils {
	class l10nstring final : public rxc::LocalizedString {
	public:
		using rxc::LocalizedString::LocalizedString;
		inline std::string localized() const {
			return _(this->posix_locale.c_str());
		}
	};

	template<long long>
	static const l10nstring* static_l10nstring(const char* str) {
		static const l10nstring ret = l10nstring((std::string)str);
		return &ret;
	}
}

#ifdef __COUNTER__
#	define L10NSTR(Msgid) ::utils::static_l10nstring<__COUNTER__>(Msgid)
#else
#	define L10NSTR(Msgid) ::utils::static_l10nstring<__LINE__>(Msgid)
#endif

#endif
