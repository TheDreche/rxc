/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/sources/file.hpp"

#include "utils/config.hpp"
#ifdef HAVE_CASSERT
#	include <cassert>
#endif
#ifdef HAVE_UTILITY
#	include <utility>
#endif

#include "rxc/configuration.hpp"
#include "rxc/sources/position.hpp"

namespace rxc::sources {
	File::File(const Configuration& config) : config(&config) {}
	File::~File() = default;

	File::Iterator File::begin() const {
		return Iterator(*this, this->begin_position());
	}
	File::Iterator File::end() const {
		return Iterator(*this, this->end_position());
	}

	// File::Iterator
	File::Iterator::Iterator(const File& file, Position&& at)
		: file(&file)
		, at(std::move(at))
	{}
	File::Iterator& File::Iterator::operator++() {
		this->file->increase(this->at);
		return *this;
	}
	const Position& File::Iterator::operator*() const {
		return this->at;
	}
	bool File::Iterator::operator!=(const Iterator& other) const {
		assert(this->file == other.file);
		return this->at != other.at;
	}
}
