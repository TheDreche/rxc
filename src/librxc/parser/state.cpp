/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/parser/state.hpp"

#include "utils/config.hpp"
#ifdef HAVE_CASSERT
#	include <cassert>
#endif
#ifdef HAVE_MEMORY
#	include <memory>
#endif
#ifdef HAVE_VECTOR
#	include <vector>
#endif

#include "rxc/lexer/file.hpp"
#include "rxc/lexer/literal_eof.hpp"
#include "rxc/lexer/position.hpp"
#include "rxc/lexer/token.hpp"

#include "rxc/sources/position.hpp"
#include "rxc/sources/span.hpp"

#include "rxc/tokenizer/position.hpp"

namespace rxc::parser {
	State::State(
		const lexer::File* file,
		const lexer::Position& position
	) : position(position), file(file) {}

	State& State::operator++() {
		this->file->increase(this->position);
		return *this;
	}

#define OPDEF(op) \
	[[nodiscard]] bool State::operator op(const State& other) const { \
		assert(this->file == other.file); \
		return this->position op other.position; \
	}

	OPDEF(==)
	OPDEF(!=)
	OPDEF(<=)
	OPDEF(>=)
	OPDEF(<)
	OPDEF(>)
#undef OPDEF

	[[nodiscard]] const lexer::Token& State::operator*() const {
		return *this->file->dereference(this->position);
	}
	[[nodiscard]] const lexer::Token* State::operator->() const {
		return this->file->dereference(this->position);
	}

	[[nodiscard]] const lexer::Position& State::getPosition() const {
		return this->position;
	}
	[[nodiscard]] const lexer::File* State::getFile() const {
		return this->file;
	}
}
