/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/parser/stage_description.hpp"

#include "utils/config.hpp"
#include "utils/gettext.hpp"

#include "rxc/export.hpp"
#include "rxc/localized_string.hpp"

#include "rxc/lexer/stage_description.hpp"

namespace rxc::parser {
	namespace stage_description {
		LIBRXC_EXPORT const LocalizedString& stage_name = *L10NSTR("Grammatical analysis");
		LIBRXC_EXPORT extern const StageDescription stage_description = StageDescription(
			stage_name,
			"Parser",
			&lexer::stage_description::stage_description
		);
	}
}
