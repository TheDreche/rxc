/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/parser/file.hpp"

#include "utils/config.hpp"
#ifdef HAVE_CSTDDEF
#	include <cstddef>
#endif
#ifdef HAVE_ITERATOR
#	include <iterator>
#endif
#ifdef HAVE_UTILITY
#	include <utility>
#endif
#ifdef HAVE_VARIANT
#	include <variant>
#endif
#include "utils/traverse_prepend.hpp"
#include "utils/variant_upcast.hpp"

#include "rxc/configuration.hpp"

#include "rxc/error/general_parser_error.hpp"
#include "rxc/error/general_parser_exception.hpp"

#include "rxc/lexer/file.hpp"
#include "rxc/lexer/literal_eof.hpp"

#include "rxc/parser/node.hpp"
#include "rxc/parser/stage_description.hpp"
#include "rxc/parser/state.hpp"

namespace rxc::parser {
	File::File(lexer::File&& file, const Configuration& config)
		: file(std::move(file))
		, content()
		, error()
	{
		// Parse tokens
		auto state = State(&this->file, *std::cbegin(this->file));
		try {
			this->content = cst::BlockContent::parseStack(config, state);

			// Add error if not at EOF
			if(!state.isAt<lexer::LiteralEof>()) {
				this->error = error::GeneralParserError(
					state.getFile(),
					state.getPosition()
				);
			}
		} catch(rxc::error::GeneralParserException exception) {
			this->error = std::move(exception.error);
		}
	}

	[[nodiscard]] const Node* File::dereference(const Position& position) const {
		std::size_t node_number = position.position;
		return this->content.traverseNode(node_number);
	}

	[[nodiscard]] const stage_description::Error* File::getError(std::size_t result_error_number) const {
		// If this->error saves a GeneralParserError, parsing was not successful.
		// Then, this is the only error.
		if(const stage_description::Error* result = std::get_if<error::GeneralParserError>(&this->error)) {
			// result doesn't need to be decremented as this is not a traversal function (taking a reference) by itself
			return result_error_number == 0? result : nullptr;
		}
		// Otherwise, parsing was "successful" (which it may also be if errors have been recovered).
		// The error saved for the whole file
		UTILS_TRAVERSE_PREPEND(
			(utils::VariantUpcast<const stage_description::Error, ErrorVariant>(this->error).value),
			result_error_number
		);
		// Traverse CST
		return this->content.traverseError(result_error_number);
	}
}
