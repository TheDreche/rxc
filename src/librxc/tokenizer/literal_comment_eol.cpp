/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/tokenizer/literal_comment_eol.hpp"

#include "utils/config.hpp"
#ifdef HAVE_MEMORY
#	include <memory>
#endif
#ifdef HAVE_UTILITY
#	include <utility>
#endif

#include "rxc/tokenizer/token.hpp"

#include "rxc/sources/file.hpp"
#include "rxc/sources/position.hpp"
#include "rxc/sources/span.hpp"

namespace rxc::tokenizer {
	LiteralCommentEol::LiteralCommentEol(const sources::Span& position) : Token(position) {}

	std::unique_ptr<const Token> LiteralCommentEol::tokenize(const sources::File& file, sources::Position& position) {
		// Check for <slash> <slash>
		sources::Position at = position;
		for(unsigned char i = 0; i < 2; ++i) {
			if(file.dereference(at) != '/') {
				return nullptr;
			}
			file.increase(at);
		}

		// Detect length
		while(
			!(
				file.dereference(at) == '\n'
				|| file.dereference(at) == '\0'
				// TODO: Warning about comment ending in EOF
			)
		) {
			file.increase(at);
		}
		sources::Position start = std::move(position);
		position = std::move(at);
		return std::make_unique<LiteralCommentEol>(
			sources::Span(start, position)
		);
	}
}
