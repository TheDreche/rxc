/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/tokenizer/literal_dot.hpp"

#include "utils/config.hpp"
#ifdef HAVE_MEMORY
#	include <memory>
#endif

#include "rxc/tokenizer/token.hpp"

#include "rxc/sources/file.hpp"
#include "rxc/sources/position.hpp"
#include "rxc/sources/span.hpp"

namespace rxc::tokenizer {
	LiteralDot::LiteralDot(const sources::Span& position) : Token(position) {}

	std::unique_ptr<const Token> LiteralDot::tokenize(const sources::File& file, sources::Position& position) {
		if(file.dereference(position) != '.') {
			return nullptr;
		}
		sources::Position start = position;
		file.increase(position);
		return std::make_unique<LiteralDot>(
			sources::Span(start, position)
		);
	}
}
