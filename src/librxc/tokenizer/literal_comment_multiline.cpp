/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/tokenizer/literal_comment_multiline.hpp"

#include "utils/config.hpp"
#ifdef HAVE_MEMORY
#	include <memory>
#endif
#ifdef HAVE_UTILITY
#	include <utility>
#endif

#include "rxc/tokenizer/token.hpp"

#include "rxc/sources/file.hpp"
#include "rxc/sources/position.hpp"
#include "rxc/sources/span.hpp"

namespace rxc::tokenizer {
	LiteralCommentMultiline::LiteralCommentMultiline(const sources::Span& position) : Token(position) {}

	std::unique_ptr<const Token> LiteralCommentMultiline::tokenize(const sources::File& file, sources::Position& position) {
		// Check for <slash> <asterisk>
		sources::Position at = position;
		if(file.dereference(at) != '/') {
			return nullptr;
		}
		file.increase(at);
		if(file.dereference(at) != '*') {
			return nullptr;
		}
		file.increase(at);

		// Detect end
		do {
			if(file.dereference(at) == '\0') {
				// TODO: Warning about comment ending in EOF
				goto finish;
			}
			while(file.dereference(at) != '*') {
				file.increase(at);
				if(file.dereference(at) == '\0') {
					goto finish;
				}
			}
			file.increase(at);
		} while(file.dereference(at) != '/');

finish:
		sources::Position start = std::move(position);
		position = std::move(at);
		return std::make_unique<LiteralCommentMultiline>(
			sources::Span(start, position)
		);
	}
}
