/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/tokenizer/file.hpp"

#include "utils/config.hpp"
#ifdef HAVE_CASSERT
#	include <cassert>
#endif
#ifdef HAVE_ITERATOR
#	include <iterator>
#endif

#include "rxc/configuration.hpp"

#include "rxc/sources/file.hpp"
#include "rxc/sources/position.hpp"
#include "rxc/sources/span.hpp"

#include "rxc/tokenizer/invalid.hpp"
#include "rxc/tokenizer/position.hpp"
#include "rxc/tokenizer/stage_description.hpp"
#include "rxc/tokenizer/token_lexer.hpp"

namespace rxc::tokenizer {
	File::File(std::unique_ptr<const sources::File>&& file)
		: read_file(std::move(file))
		, tokens()
	{
		this->lex_all();
	}

	const stage_description::Error* File::getError(std::size_t error_number) const {
		this->lex_all();
		for(const TokenWrapper& token : this->tokens) {
			const stage_description::Error* error = token->traverseError(error_number);
			if(error != nullptr) {
				return error;
			}
		}
		return nullptr;
	}

	File::Iterator File::begin() const {
		return File::Iterator(std::cbegin(this->tokens));
	}
	File::Iterator File::end() const {
		return File::Iterator(std::cend(this->tokens));
	}

	void File::increase(Position& position) const {
		if(this->tokens.empty()) {
			this->lex(position.position);
		} else {
			auto it = std::cbegin(this->tokens);
			while((*it)->start <= position.position) {
				++it;
			}
			position.position = (*it)->start;
		}
	}
	const Token* File::dereference(const Position& position) const {
		this->lex_all();
		auto it = this->tokens.begin();
		while((*it)->start < position.position) {
			++it;
		}
		assert((*it)->start == position.position);
		return (*it).get();
	}

	File::TokenWrapper File::lex(sources::Position& at) const {
		TokenWrapper result = nullptr;

#ifndef NDEBUG
		sources::Position pos_start = at;
#endif
		for(TokenLexer tokenizer : this->read_file->config->token_lexers) {
			result = tokenizer(*this->read_file, at);
			if(result != nullptr) {
				break;
			} else {
#ifndef NDEBUG
				assert(at == pos_start);
#endif
			}
		}
		if(result == nullptr) {
#ifdef NDEBUG
			sources::Position pos_start = at;
#else
			// We already have the same variable one scope above
#endif
			while(true) {
				this->read_file->increase(at);
				sources::Position pos_prev = at;
				for(TokenLexer tokenizer : this->read_file->config->token_lexers) {
					result = tokenizer(*this->read_file, at);
					if(result != nullptr) {
						break;
					} else {
						assert(at == pos_prev);
					}
				}
				if(result != nullptr) {
					at = std::move(pos_prev);
					break;
				}
			}
			result = std::make_unique<Invalid>(this->read_file.get(), sources::Span(pos_start, at));
		}
		assert(result != nullptr);
		return result;
	}

	void File::lex_all() const {
		if(!this->tokens.empty()) {
			return;
		}

		sources::Position at = this->read_file->begin_position();
		sources::Position end = this->read_file->end_position();

		while (!(at == end)) {
			std::unique_ptr<const Token> token = this->lex(at);
			if(token != nullptr) {
				this->tokens.push_back(std::move(token));
			} else {
				// TODO: Error
				this->read_file->increase(at);
			}
		}

		this->tokens.shrink_to_fit();
	}

	// Iterator
	File::Iterator::Iterator(TokenVector::const_iterator&& iter)
		: iter(std::move(iter))
	{}

	Position File::Iterator::operator*() const {
		return Position((*this->iter)->start);
	}

	File::Iterator& File::Iterator::operator++() {
		++this->iter;
		return *this;
	}

	bool File::Iterator::operator==(const Iterator& other) const {
		return this->iter == other.iter;
	}
	bool File::Iterator::operator!=(const Iterator& other) const {
		return !(*this == other);
	}
}
