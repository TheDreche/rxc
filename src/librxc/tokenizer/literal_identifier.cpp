/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/tokenizer/literal_identifier.hpp"

#include "utils/config.hpp"
#ifdef HAVE_MEMORY
#	include <memory>
#endif
#ifdef HAVE_STRING
#	include <string>
#endif
#ifdef HAVE_UTILITY
#	include <utility>
#endif
#include "utils/char_contains.hpp"

#include "rxc/tokenizer/token.hpp"

#include "rxc/sources/file.hpp"
#include "rxc/sources/position.hpp"
#include "rxc/sources/span.hpp"

static inline bool isIdentifierChar(char c) {
	return utils::charContains(c,
		"abcdefghijklmnopqrstuvwxyz"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"0123456789_"
	);
}

namespace rxc::tokenizer {
	LiteralIdentifier::LiteralIdentifier(
		const sources::Span& position,
		std::string&& name
	) : Token(position), name(std::move(name)) {}

	std::unique_ptr<const Token> LiteralIdentifier::tokenize(
		const sources::File& file,
		sources::Position& position
	) {
		sources::Position start = position;
		std::string name = "";

		{
			char current_char;
			while(isIdentifierChar(current_char = file.dereference(position))) {
				name += current_char;
				file.increase(position);
			}
		}

		if(name.empty()) {
			return nullptr;
		} else {
			name.shrink_to_fit();
		}

		return std::make_unique<LiteralIdentifier>(
			sources::Span(start, position),
			std::move(name)
		);
	}
}
