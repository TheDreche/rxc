/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/tokenizer/token.hpp"

#include "utils/config.hpp"
#ifdef HAVE_CSTDDEF
#	include <cstddef>
#endif

#include "rxc/sources/span.hpp"
#include "rxc/tokenizer/stage_description.hpp"

namespace rxc::tokenizer {
	Token::Token(const sources::Span& start) : start(start.start) {}
	Token::~Token() = default;

	const stage_description::Error* Token::traverseError(std::size_t&) const {
		return nullptr;
	}
}
