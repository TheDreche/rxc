/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/error/general_parser_error.hpp"

#include "utils/config.hpp"
#ifdef HAVE_CSTDDEF
#	include <cstddef>
#endif
#include "utils/gettext.hpp"

#include "rxc/localized_string.hpp"
#include "rxc/parser/stage_description.hpp"

#include "rxc/error/error_levels.hpp"
#include "rxc/error/error_staged.hpp"
#include "rxc/error/error_staged_description.hpp"
#include "rxc/error/error_type.hpp"

static const rxc::LocalizedString* desc_text = L10NSTR("Unexpected token");

namespace rxc::error {
	// GeneralParserError
	const ErrorType GeneralParserError::error_type = ErrorType {
		.name = L10NSTR("Syntax error"),
		.configuration_name = "general_parser_error",
		.level = &error_levels::error,
		.stage = &parser::stage_description::stage_description,
	};

	GeneralParserError::GeneralParserError(
		const EDescription::File* file,
		const EDescription::Position& position
	) : ErrorStaged(), error_description(file, position) {}

	const ErrorType* GeneralParserError::type() const {
		return &error_type;
	}

	const GeneralParserError::Description* GeneralParserError::staged_description() const {
		return &this->error_description;
	}

	const GeneralParserError::Hint* GeneralParserError::staged_hint(std::size_t) const {
		return nullptr;
	}

	const GeneralParserError::Suggestion* GeneralParserError::staged_suggestion(std::size_t) const {
		return nullptr;
	}

	// GeneralParserError::EDescription
	GeneralParserError::EDescription::EDescription(const File* file, const Position& position)
		: ErrorStagedDescription(file, position)
	{}

	const LocalizedString& GeneralParserError::EDescription::text() const {
		return *desc_text;
	}
}
