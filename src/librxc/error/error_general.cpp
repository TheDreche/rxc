/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/error/error_general.hpp"

#include "utils/config.hpp"

#include "rxc/iter/member_getter.hpp"

#include "rxc/error/error_general_hint.hpp"
#include "rxc/error/error_general_suggestion.hpp"

namespace rxc::error {
	ErrorGeneral::ErrorGeneral() = default;
	ErrorGeneral::~ErrorGeneral() = default;

	iter::MemberGetter<const ErrorGeneralHint, ErrorGeneral> ErrorGeneral::hints() const {
		return iter::MemberGetter<const ErrorGeneralHint, ErrorGeneral>(
			this,
			&ErrorGeneral::hint
		);
	}

	iter::MemberGetter<const ErrorGeneralSuggestion, ErrorGeneral> ErrorGeneral::suggestions() const {
		return iter::MemberGetter<const ErrorGeneralSuggestion, ErrorGeneral>(
			this,
			&ErrorGeneral::suggestion
		);
	}
}
