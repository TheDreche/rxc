/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/error/error_suggestion.hpp"

#include "utils/config.hpp"
#ifdef HAVE_STRING
#	include <string>
#endif
#ifdef HAVE_UTILITY
#	include <utility>
#endif

#include "rxc/sources/file.hpp"
#include "rxc/sources/span.hpp"

namespace rxc::error {
	ErrorSuggestion::ErrorSuggestion(
		const sources::File* file,
		const sources::Span& position,
		std::string&& replacement
	)	: file(file)
		, position(position)
		, replacement(std::move(replacement))
	{}
}
