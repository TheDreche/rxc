/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/error/invalid_token_start_error.hpp"

#include "utils/config.hpp"
#ifdef HAVE_CSTDDEF
#	include <cstddef>
#endif
#include "utils/gettext.hpp"

#include "rxc/localized_string.hpp"
#include "rxc/tokenizer/stage_description.hpp"

#include "rxc/error/error_levels.hpp"
#include "rxc/error/error_staged.hpp"
#include "rxc/error/error_staged_description.hpp"
#include "rxc/error/error_type.hpp"

static const rxc::LocalizedString* desc_text = L10NSTR("Invalid token start sequence");

namespace rxc::error {
	// InvalidTokenStartError
	const ErrorType InvalidTokenStartError::error_type = ErrorType {
		.name = L10NSTR("Invalid token start"),
		.configuration_name = "invalid_token_start",
		.level = &error_levels::error,
		.stage = &tokenizer::stage_description::stage_description,
	};

	InvalidTokenStartError::InvalidTokenStartError(
		const EDescription::File* file,
		const EDescription::Position& position
	) : ErrorStaged(), error_description(file, position) {}

	const ErrorType* InvalidTokenStartError::type() const {
		return &error_type;
	}

	const InvalidTokenStartError::Description* InvalidTokenStartError::staged_description() const {
		return &this->error_description;
	}

	const InvalidTokenStartError::Hint* InvalidTokenStartError::staged_hint(std::size_t) const {
		return nullptr;
	}

	const InvalidTokenStartError::Suggestion* InvalidTokenStartError::staged_suggestion(std::size_t) const {
		return nullptr;
	}

	// InvalidTokenStartError::EDescription
	InvalidTokenStartError::EDescription::EDescription(const File* file, const Position& position)
		: ErrorStagedDescription(file, position)
	{}

	const LocalizedString& InvalidTokenStartError::EDescription::text() const {
		return *desc_text;
	}
}
