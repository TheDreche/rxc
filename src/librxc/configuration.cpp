/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/configuration.hpp"

#include "utils/config.hpp"
#ifdef HAVE_FUNCTIONAL
#	include <functional>
#endif
#ifdef HAVE_MEMORY
#	include <memory>
#endif

#include "rxc/parser/state.hpp"

#include "rxc/cst/block.hpp"
#include "rxc/cst/block_comma.hpp"
#include "rxc/cst/constant_declaration.hpp"
#include "rxc/cst/constant_definition.hpp"
#include "rxc/cst/constant_local.hpp"
#include "rxc/cst/constant_qualified.hpp"
#include "rxc/cst/expression.hpp"
#include "rxc/cst/function_call.hpp"
#include "rxc/cst/function_call_unnamed.hpp"
#include "rxc/cst/function_definition.hpp"
#include "rxc/cst/function_type.hpp"
#include "rxc/cst/namespace_reference_absolute.hpp"
#include "rxc/cst/type_definition.hpp"
#include "rxc/cst/type_definition_instance.hpp"
#include "rxc/cst/type_definition_property.hpp"
#include "rxc/cst/type_definition_statement.hpp"
#include "rxc/cst/type_instanciation.hpp"
#include "rxc/cst/type_instanciation_instance.hpp"
#include "rxc/cst/type_instanciation_property.hpp"

#include "rxc/tokenizer/literal_colon.hpp"
#include "rxc/tokenizer/literal_comma.hpp"
#include "rxc/tokenizer/literal_comment_eol.hpp"
#include "rxc/tokenizer/literal_comment_multiline.hpp"
#include "rxc/tokenizer/literal_curly_bracket_left.hpp"
#include "rxc/tokenizer/literal_curly_bracket_right.hpp"
#include "rxc/tokenizer/literal_dot.hpp"
#include "rxc/tokenizer/literal_eof.hpp"
#include "rxc/tokenizer/literal_equals.hpp"
#include "rxc/tokenizer/literal_identifier.hpp"
#include "rxc/tokenizer/literal_parentheses_left.hpp"
#include "rxc/tokenizer/literal_parentheses_right.hpp"
#include "rxc/tokenizer/literal_semicolon.hpp"
#include "rxc/tokenizer/literal_whitespace.hpp"

#include "rxc/lexer/discard.hpp"
#include "rxc/lexer/invalid.hpp"
#include "rxc/lexer/literal_colon.hpp"
#include "rxc/lexer/literal_comma.hpp"
#include "rxc/lexer/literal_curly_bracket_left.hpp"
#include "rxc/lexer/literal_curly_bracket_right.hpp"
#include "rxc/lexer/literal_dot.hpp"
#include "rxc/lexer/literal_equals.hpp"
#include "rxc/lexer/literal_identifier.hpp"
#include "rxc/lexer/literal_parentheses_left.hpp"
#include "rxc/lexer/literal_parentheses_right.hpp"
#include "rxc/lexer/literal_semicolon.hpp"

template<class Result, class From>
static std::unique_ptr<Result> parseNode(
	const rxc::Configuration& config,
	rxc::parser::State& state
) {
	return From::parse(config, state);
}

namespace rxc {
	Configuration Configuration::defaultConfiguration() {
		return {
			.token_lexers = {
				std::function(&tokenizer::LiteralColon::tokenize),
				std::function(&tokenizer::LiteralComma::tokenize),
				std::function(&tokenizer::LiteralCommentEol::tokenize),
				std::function(&tokenizer::LiteralCommentMultiline::tokenize),
				std::function(&tokenizer::LiteralCurlyBracketLeft::tokenize),
				std::function(&tokenizer::LiteralCurlyBracketRight::tokenize),
				std::function(&tokenizer::LiteralDot::tokenize),
				std::function(&tokenizer::LiteralEof::tokenize),
				std::function(&tokenizer::LiteralEquals::tokenize),
				std::function(&tokenizer::LiteralIdentifier::tokenize),
				std::function(&tokenizer::LiteralParenthesesLeft::tokenize),
				std::function(&tokenizer::LiteralParenthesesRight::tokenize),
				std::function(&tokenizer::LiteralSemicolon::tokenize),
				std::function(&tokenizer::LiteralWhitespace::tokenize),
			},
			.token_transformers = {
				std::function(&lexer::discard<tokenizer::LiteralCommentEol>),
				std::function(&lexer::discard<tokenizer::LiteralCommentMultiline>),
				std::function(&lexer::discard<tokenizer::LiteralWhitespace>),
				std::function(&lexer::Invalid::transform),
				std::function(&lexer::LiteralColon::transform),
				std::function(&lexer::LiteralComma::transform),
				std::function(&lexer::LiteralCurlyBracketLeft::transform),
				std::function(&lexer::LiteralCurlyBracketRight::transform),
				std::function(&lexer::LiteralDot::transform),
				std::function(&lexer::LiteralEquals::transform),
				std::function(&lexer::LiteralIdentifier::transform),
				std::function(&lexer::LiteralParenthesesLeft::transform),
				std::function(&lexer::LiteralParenthesesRight::transform),
				std::function(&lexer::LiteralSemicolon::transform),
			},
			.expression_parsers = {
				std::function(&parseNode<cst::Expression, cst::Block>),
				std::function(&parseNode<cst::Expression, cst::FunctionDefinition>),
				std::function(&parseNode<cst::Expression, cst::FunctionType>),
				std::function(&parseNode<cst::Expression, cst::TypeDefinition>),
				std::function(&parseNode<cst::Expression, cst::ConstantQualified>),
				std::function(&parseNode<cst::Expression, cst::ConstantLocal>),
			},
			.expression_postfix_operator_parsers = {
				std::function(&cst::FunctionCall::parsePostfix<cst::Expression>),
				std::function(&cst::TypeInstanciation::parsePostfix<cst::Expression>),
				std::function(&cst::FunctionCallUnnamed::parsePostfix<cst::Expression>),
			},
			.block_content_expression_parsers = {
				std::function(&cst::Expression::parse),
			},
			.block_content_expression_postfix_parsers = {
				std::function(&cst::BlockComma::parsePostfix<cst::Expression>),
			},
			.namespace_reference_parsers = {
				std::function(&parseNode<cst::NamespaceReference, cst::NamespaceReferenceAbsolute>),
				std::function(&parseNode<cst::NamespaceReference, cst::NamespaceReferenceRelative>),
			},
			.block_statement_parsers = {
				std::function(&parseNode<cst::BlockStatement, cst::ConstantDeclaration>),
				std::function(&parseNode<cst::BlockStatement, cst::ConstantDefinition>),
			},
			.block_statement_postfix_parsers = {},
			.type_definition_statement_parsers = {
				std::function(&parseNode<cst::TypeDefinitionStatement, cst::TypeDefinitionProperty>),
				std::function(&parseNode<cst::TypeDefinitionStatement, cst::TypeDefinitionInstance>),
			},
			.type_definition_statement_postfix_parsers = {},
			.type_instanciation_statement_parsers = {
				std::function(&parseNode<cst::TypeInstanciationStatement, cst::TypeInstanciationProperty>),
				std::function(&parseNode<cst::TypeInstanciationStatement, cst::TypeInstanciationInstance>),
			},
			.type_instanciation_statement_postfix_parsers = {},
		};
	}
}
