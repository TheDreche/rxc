/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/cst/block_content.hpp"

#include "utils/config.hpp"

#include "rxc/export.hpp"
#include "rxc/cst/expression.hpp"

#include "rxc/parser/parser_heap.hpp"
#include "rxc/parser/parse_decision.hpp"

namespace rxc::cst {
	namespace _internal::_BlockContent {
		LIBRXC_EXPORT extern const parser::ParserHeap<Expression> parseExpression = &parser::parseDecision<
			Expression,
			&rxc::Configuration::block_content_expression_parsers,
			&rxc::Configuration::block_content_expression_postfix_parsers
		>;
	}

	BlockContent::BlockContent() = default;
}
