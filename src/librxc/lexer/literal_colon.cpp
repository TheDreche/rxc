/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/lexer/literal_colon.hpp"

#include "utils/config.hpp"
#ifdef HAVE_MEMORY
#	include <memory>
#endif
#ifdef HAVE_VECTOR
#	include <vector>
#endif

#include "rxc/tokenizer/file.hpp"
#include "rxc/tokenizer/literal_colon.hpp"
#include "rxc/tokenizer/position.hpp"

namespace rxc::lexer {
	LiteralColon::LiteralColon(const tokenizer::Position& position)
		: Token(position)
	{}

	void LiteralColon::transform(
		const tokenizer::File& file,
		tokenizer::Position& position,
		std::vector<std::unique_ptr<const Token>>& result
	) {
		if(file.dereference(position)->isType<tokenizer::LiteralColon>()) {
			tokenizer::Position start = position;
			file.increase(position);
			result.push_back(std::make_unique<LiteralColon>(start));
		}
	}
}
