/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rxc/lexer/file.hpp"

#include "utils/config.hpp"
#ifdef HAVE_CSTDDEF
#	include <cstddef>
#endif
#ifdef HAVE_MEMORY
#	include <memory>
#endif
#ifdef HAVE_UTILITY
#	include <utility>
#endif

#include "rxc/configuration.hpp"

#include "rxc/tokenizer/file.hpp"
#include "rxc/tokenizer/literal_eof.hpp"
#include "rxc/tokenizer/position.hpp"

#include "rxc/lexer/literal_eof.hpp"
#include "rxc/lexer/position.hpp"
#include "rxc/lexer/stage_description.hpp"
#include "rxc/lexer/token_transformer.hpp"

namespace rxc::lexer {
	File::File(std::unique_ptr<const tokenizer::File>&& file)
		: tokenized(std::move(file))
		, tokens()
	{
		this->lex_all();
	}

	const stage_description::Error* File::getError(std::size_t) const {
		return nullptr;
	}

	File::Iterator File::begin() const {
		return File::Iterator(Position(0));
	}
	File::Iterator File::end() const {
		return File::Iterator(Position(this->tokens.size()));
	}

	void File::increase(Position& position) const {
		++position.position;
	}
	const Token* File::dereference(const Position& position) const {
		this->lex_all();
		return this->tokens[position.position].get();
	}

	void File::lex_all() const {
		if(!this->tokens.empty()) {
			return;
		}

		tokenizer::Position at = *this->tokenized->begin();
		while(!this->tokenized->dereference(at)->isType<tokenizer::LiteralEof>()) {
			tokenizer::Position pos_start = at;
			for(const TokenTransformer& transformer : this->tokenized->read_file->config->token_transformers) {
				transformer(*this->tokenized, at, this->tokens);
				// Do not compare the length of the token list as there may be patterns for discarding tokens completely!
				if(at != pos_start) {
					break;
				}
			}
			assert(at != pos_start);
		}
		this->tokens.push_back(std::make_unique<LiteralEof>(at));

		this->tokens.shrink_to_fit();
	}

	// Iterator
	File::Iterator::Iterator(Position&& pos)
		: position(std::move(pos))
	{}

	Position File::Iterator::operator*() const {
		return this->position;
	}

	File::Iterator& File::Iterator::operator++() {
		++this->position.position;
		return *this;
	}

	bool File::Iterator::operator==(const Iterator& other) const {
		return this->position == other.position;
	}
	bool File::Iterator::operator!=(const Iterator& other) const {
		return !(*this == other);
	}
}
