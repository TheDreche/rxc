/* Copyright (C) 2021-2023 Dreche
 * 
 * This file is part of rxcc.
 * 
 * rxcc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * rxcc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with rxcc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils/config.hpp"
#ifdef HAVE_CASSERT
#	include <cassert>
#endif
#ifdef HAVE_CSTDDEF
#	include <cstddef>
#endif
#ifdef HAVE_FILESYSTEM
#	include <filesystem>
#endif
#ifdef HAVE_FSTREAM
#	include <fstream>
#endif
#ifdef HAVE_IOS
#	include <ios>
#endif
#ifdef HAVE_MEMORY
#	include <memory>
#endif
#include "utils/unreachable.hpp"

#include "rxc.hpp"

#include "sources.hpp"

// TODO: Errors

namespace rxcc::sources {
	// File
	File::File(const rxc::Configuration& config, const std::filesystem::path& file)
		: rxc::sources::File(config)
		, stream(file)
	{}

	const rxc::error::ErrorGeneral* File::getError(std::size_t) const {
		return nullptr;
	}

	rxc::sources::Position File::begin_position() const {
		return rxc::sources::Position(0);
	}

	rxc::sources::Position File::end_position() const {
		this->stream.clear();
		this->stream.seekg(0, std::ios_base::end);
		return rxc::sources::Position(
			std::streamoff(this->stream.tellg()) + 1
		);
	}

	char File::dereference(const rxc::sources::Position& position) const {
		this->stream.clear();
		this->stream.seekg(position.position);
		auto r = this->stream.peek();
		if(r == std::ifstream::traits_type::eof()) {
			return '\0';
		}
		return r;
	}

	void File::increase(rxc::sources::Position& position) const {
		++position.position;
	}
}
