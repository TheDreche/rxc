/* Copyright (C) 2021-2023 Dreche
 * 
 * This file is part of rxcc.
 * 
 * rxcc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * rxcc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with rxcc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXCC_SOURCES_HPP
#define RXCC_SOURCES_HPP

#include "utils/config.hpp"
#ifdef HAVE_CSTDDEF
#	include <cstddef>
#endif
#ifdef HAVE_FILESYSTEM
#	include <filesystem>
#endif
#ifdef HAVE_FSTREAM
#	include <fstream>
#endif
#ifdef HAVE_IOS
#	include <ios>
#endif
#ifdef HAVE_MEMORY
#	include <memory>
#endif
#include "utils/export.hpp"

#include "rxc.hpp"

namespace rxcc::sources {
	class EXPORT File final : public rxc::sources::File {
	private:
		mutable std::ifstream stream;

	public:
		File(const rxc::Configuration&, const std::filesystem::path&);
		virtual const rxc::error::ErrorGeneral* getError(std::size_t) const override;

	public:
		virtual rxc::sources::Position begin_position() const final override;
		virtual rxc::sources::Position end_position() const final override;
		virtual rxc::sources::Character dereference(const rxc::sources::Position&) const final override;
		virtual void increase(rxc::sources::Position&) const final override;
	};
}

#endif
