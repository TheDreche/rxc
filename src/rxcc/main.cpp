/* Copyright (C) 2021-2023 Dreche
 * 
 * This file is part of rxcc.
 * 
 * rxcc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * rxcc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with rxcc.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils/config.hpp"
#ifdef HAVE_CSTDDEF
#	include <cstddef>
#endif
#ifdef HAVE_CSTDIO
#	include <cstdio>
#endif
#ifdef HAVE_CLOCALE
#	include <clocale>
#endif
#ifdef HAVE_GETOPT_H
#	include <getopt.h>
#endif

#ifdef HAVE_FILESYSTEM
#	include <filesystem>
#endif
#ifdef HAVE_MEMORY
#	include <memory>
#endif
#ifdef HAVE_VECTOR
#	include <vector>
#endif
#include <fmt/color.h>
#include <fmt/compile.h>
#include <fmt/core.h>
#include "utils/gettext.hpp"

#include "rxc.hpp"

#include "sources.hpp"

enum cli_action {
	ACTION_DEFAULT,
	ACTION_COMPILE,
	ACTION_PRINT_HELP,
	ACTION_PRINT_VERSION,
	ACTION_ERROR_USAGE
};

class options {
	public:
		const char* self;
		cli_action action = ACTION_DEFAULT;
		std::vector<std::filesystem::path> input_files;
};

void parse_cli_options(int argc, char** argv, options& to) {
	to.self = argv[0];
	
	enum option_id : int {
		OPTION_HELP,
		OPTION_VERSION
	};
	int found_option;
	static option long_options[] = {
		option{"help",    no_argument, &found_option, OPTION_HELP},
		option{"version", no_argument, &found_option, OPTION_VERSION},
		option{NULL,      0,           NULL, 0}
	};
	
	while(true) {
		int option_index;
		int r = getopt_long(argc, argv, ":hv", long_options, &option_index);
		if(r == -1) {
			break;
		}
		switch((char) r) {
			case '?':
			case ':':
				to.action = ACTION_ERROR_USAGE;
				break;
			case 'h':
				found_option = OPTION_HELP;
				break;
			case 'v':
				found_option = OPTION_VERSION;
				break;
		}
		if(to.action == ACTION_ERROR_USAGE) {
			// Don't bother with actually checking the options if invalid either way
			continue;
		}
		switch((option_id) found_option) {
			case OPTION_HELP:
				if(to.action != ACTION_DEFAULT) {
					to.action = ACTION_ERROR_USAGE;
				} else {
					to.action = ACTION_PRINT_HELP;
				}
				break;
			case OPTION_VERSION:
				if(to.action != ACTION_DEFAULT) {
					to.action = ACTION_ERROR_USAGE;
				} else {
					to.action = ACTION_PRINT_VERSION;
				}
				break;
		}
	}
	while(argv[optind] != nullptr) {
		to.input_files.emplace_back(argv[optind]);
		++optind;
	}
}

void usage(const char* self) {
	fmt::print(_(
		"Usage: {} [options] file ...\n"
		"Options:\n"
		"  --help       Print this help message.\n"
		"  --version    Display version information.\n"
		"The options --help and --version are incompatible. --help will preceed.\n"
	), self);
}

void version() {
	fmt::print("rxcc ({}) {}\n{}", PACKAGE_NAME, PACKAGE_VERSION, _(
		"Copyright (C) 2022 Dreche\n"
		"License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>\n"
		"This is free software: you are free to change and redistribute it.\n"
		"There is NO WARRANTY, to the extent permitted by law.\n"
	));
}

void localize() {
	std::setlocale(LC_ALL, "");
	bindtextdomain(PACKAGE, LOCALEDIR);
}

template<class T>
T copy(const T& o) {
	return o;
}

int compile(options& cli) {
	rxc::Configuration config = rxc::Configuration::defaultConfiguration();
	for(std::filesystem::path& input_file : cli.input_files) {
		fmt::print("{}\n", input_file.c_str());

		try {
			auto file = rxc::parser::File(
				rxc::lexer::File(
					std::make_unique<rxc::tokenizer::File>(
						std::make_unique<rxcc::sources::File>(config, copy(input_file))
					)
				),
				config
			);

			// Print tokenizer errors
			const rxc::error::ErrorGeneral* error;
			for(std::size_t i = 0; (error = file.file.getError(i)) != nullptr; ++i) {
				fmt::print(
					stderr,
					fmt::fg(fmt::color::red) | fmt::emphasis::bold,
					"{}: {}\n",
					error->type()->level->name->localized(),
					error->type()->name->localized()
				);
			}
			// Print parser errors
			for(std::size_t i = 0; (error = file.getError(i)) != nullptr; ++i) {
				fmt::print(
					stderr,
					fmt::fg(fmt::color::red) | fmt::emphasis::bold,
					"{}: {}\n",
					error->type()->level->name->localized(),
					error->type()->name->localized()
				);
			}
		} catch(...) {
			fmt::print(stderr, fmt::fg(fmt::color::red) | fmt::emphasis::bold, _("Severe error: Cannot open file {}\n"), input_file.c_str());
			continue;
		}
	}
	fmt::print(stderr, "Compiling has not been implemented up to now ...\n");
	return EXIT_FAILURE;
}

int main(int argc, char** argv) {
	options opts;

	rxc::initialize();
	localize();
	parse_cli_options(argc, argv, opts);
	
	switch(opts.action) {
		case ACTION_DEFAULT:
		case ACTION_COMPILE:
			return compile(opts);
		case ACTION_PRINT_VERSION:
			version();
			return EXIT_SUCCESS;
		case ACTION_PRINT_HELP:
			usage(opts.self);
			return EXIT_SUCCESS;
		case ACTION_ERROR_USAGE:
			usage(opts.self);
			return EXIT_FAILURE;
	}
}
