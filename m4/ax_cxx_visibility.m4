# AX_CXX_VISIBILITY
# -----------------
AC_DEFUN([AX_CXX_VISIBILITY], [
	AC_CACHE_CHECK([whether the compiler understands the visibility function attribute], ax_cv_cxx_visibility,
	[AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],
[[
__attribute__((visibility("default"))) int theAnswer() {
	return 42;
}

int main() {
	return theAnswer();
}
]])],
		   [ax_cv_cxx_visibility=no],
		   [ax_cv_cxx_visibility=yes])])
	AS_IF([test "x${ax_cv_cxx_visibility}" = "xyes"], [
		AC_DEFINE([HAVE_VISIBILITY], [1], [Define to 1 if the compiler supports the visibility function attribute.])
	])
])# AX_CXX_VISIBILITY

