dnl ++++++++++++++++++++++++++++++++++++++++++++++
dnl +                                            +
dnl +         Initializing some programs         +
dnl +     ---============================---     +
dnl +                                            +
dnl ++++++++++++++++++++++++++++++++++++++++++++++

dnl +---------------+
dnl | Init Autoconf |
dnl +---------------+
AC_PREREQ([2.69])
AC_INIT([rxc], [0.4.0])

dnl | General notes
dnl +---------------
AC_COPYRIGHT([Copyright (C) 2022 Dreche])
AC_REVISION([m4_esyscmd([./templates/verinfo/commitof.sh --modified=-dirty --newline=off --verinfo=verinfo --fallback=unknown -- configure.ac])])

dnl | Configuration
dnl +---------------
AC_CONFIG_SRCDIR([include/rxc.hpp])
AC_CONFIG_MACRO_DIRS([
	m4
	templates/doxygen/m4
])
AC_CONFIG_AUX_DIR([build-aux])
AC_LANG([C++])

dnl | Output
dnl +--------
dnl | Variables
AC_SUBST(CFLAG_VISIBILITY)
AC_SUBST(PACKAGE_DESCRIPTION)

dnl | Variable values
PACKAGE_DESCRIPTION='Rusty eXtended C'

dnl | Files
AC_CONFIG_FILES([
	Makefile
	po/Makefile.in
	pkgconfig/librxc.pc
])


dnl +---------------+
dnl | Init Automake |
dnl +---------------+
AM_INIT_AUTOMAKE([1.13 subdir-objects])

dnl | Configuration
dnl +---------------
AM_SILENT_RULES([yes])


dnl +------------+
dnl | Autoheader |
dnl +------------+
dnl | Files
dnl +-------
AC_CONFIG_HEADERS([src/utils/utils/config.h])

dnl | Templates
dnl +-----------
AH_TEMPLATE([NDEBUG], [Define to 1 if debug code should NOT be compiled])


dnl +--------------+
dnl | Init LibTool |
dnl +--------------+
LT_INIT()


dnl +--------------+
dnl | Init gettext |
dnl +--------------+
AM_GNU_GETTEXT_VERSION([0.21])
AM_GNU_GETTEXT([external])

dnl +-----------------+
dnl | Init PKG-CONFIG |
dnl +-----------------+
PKG_PROG_PKG_CONFIG()
PKG_INSTALLDIR()


dnl ++++++++++++++++++++++++++++++++
dnl +                              +
dnl +         Some Options         +
dnl +     ---==============---     +
dnl +                              +
dnl ++++++++++++++++++++++++++++++++

dnl +---------+
dnl | General |
dnl +---------+
AC_ARG_ENABLE(
	[debug],
	[AS_HELP_STRING([--enable-debug], [enable some features useful for debugging @<:@default=no@:>@])],
[
	AS_CASE(["$enableval"],
		[yes], [enable_debug=yes],
		[no], [enable_debug=no],
		[AC_MSG_ERROR(
			[bad value ${enableval} for --enable-debug]
		)]
	)
], [
	enable_debug="no"
])

dnl | Documentation
dnl +---------------
dnl | Does everything needed for documentation
AX_TEMPLATE_DOXYGEN([doc], [templates/doxygen])


dnl +++++++++++++++++++++++++++++++
dnl +                             +
dnl +         Some checks         +
dnl +     ---=============---     +
dnl +                             +
dnl +++++++++++++++++++++++++++++++

dnl +-----------------------------------+
dnl | For programs and features of them |
dnl +-----------------------------------+
dnl | OS Large file support
dnl +-----------------------
AC_SYS_LARGEFILE()

dnl | CC
dnl +----
AC_PROG_CC()

dnl | CXX (c++)
dnl +-----------
AC_PROG_CXX()
AX_CXX_COMPILE_STDCXX([17])
AX_CXX_OVERRIDE()
AX_CXX_VISIBILITY()
AX_CHECK_COMPILE_FLAG([-fvisibility=hidden], [CFLAG_VISIBILITY='-fvisibility=hidden'], [CFLAG_VISIBILITY=''])
dnl Compile all public if visibility attribute is not supported
AS_IF([test "x$ax_cv_cxx_visibility" = "xno"], [CFLAG_VISIBILITY=''])

dnl | Headers
dnl +---------
AC_CHECK_HEADERS_ONCE([
	algorithm
	cassert
	clocale
	cstddef
	cstdint
	cstdlib
	cstring
	filesystem
	fstream
	functional
	ios
	istream
	iterator
	limits
	memory
	optional
	stack
	string
	type_traits
	unordered_map
	unordered_set
	variant
	vector
	getopt.h
])

dnl | Librarys
dnl +----------
dnl | gmpxx
dnl +-------
PKG_CHECK_MODULES([GMPXX], [gmpxx], [
	GMPXX_VERSION="$(${PKG_CONFIG} --modversion gmpxx)"
], [
	AC_MSG_ERROR([GMPXX (gmp for C++) required but not found])
])

dnl | fmt
dnl +-----
PKG_CHECK_MODULES([FMT], [fmt], [
	FMT_VERSION="$(${PKG_CONFIG} --modversion fmt)"
], [
	AC_MSG_ERROR([FMT required but not found])
])

dnl +-------------------------------+
dnl | Whether C++ works as expected |
dnl +-------------------------------+
AC_CACHE_CHECK([for required C++ features], ax_cv_local_all_features_cxx, [
	AC_RUN_IFELSE(
		[AC_LANG_PROGRAM(
[[
#include <filesystem>
#include <unordered_set>
#include <cassert>
]], [[
auto pwd = std::filesystem::current_path();
assert(pwd.is_absolute());
auto set = std::unordered_set<std::filesystem::path>();
set.insert(pwd);
]]
		)],
		[ax_cv_local_all_features_cxx=yes],
		[ax_cv_local_all_features_cxx=no],
		[ax_cv_local_all_features_cxx=crosscompile]
	)
])
AS_IF([test "x$ax_cv_local_all_features_cxx" = "xno"], [
	AC_MSG_ERROR([C++ compiler/STL is missing at least one required feature])
])


dnl ++++++++++++++++++++++++++
dnl +                        +
dnl +         Output         +
dnl +     ---========---     +
dnl +                        +
dnl ++++++++++++++++++++++++++

dnl +-------------------------+
dnl | C++ compilation symbols |
dnl +-------------------------+
dnl | NDEBUG
dnl +--------
AS_IF([test "x$enable_debug" = "xno"],
	[AC_DEFINE([NDEBUG], [1])]
)


dnl +-----------+
dnl | All files |
dnl +-----------+
AC_OUTPUT()

dnl +----------------+
dnl | To the console |
dnl +----------------+
AS_ECHO("
Configuration results:
  RXCC ................ ${VERSION}
  
  target .............. ${with_target}
    prefix ............ ${prefix}
    SYS ............... ${SYS}
    ARCH .............. ${ARCH}
  
  HOST ................. ${HOST}
    HOSTNAME ........... ${HOSTNAME}
    host ............... ${host}
  
  INSTALL .............. ${INSTALL}
  
  CC ................... ${CC}
    CFLAGS ............. ${CFLAGS}
    CPP ................ ${CPP}
      CPPFLAGS ......... ${CPPFLAGS}
  
  CXX .................. ${CXX}
    CXXFLAGS ........... ${CXXFLAGS}
    CXXCPP ............. ${CXXCPP}
      CXXCPPFLAGS ...... ${CXXCPPFLAGS}
  
  GMPXX ................ ${GMPXX_VERSION}
  FMT .................. ${FMT_VERSION}
  
  Flags passed to configure:
    --enable-debug ..... ${enable_debug}
")
