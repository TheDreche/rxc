# RXC

This is a compiler for [dcl](https://gitlab.com/TheDreche/dlang), a minimalistic purely functional programming language supporting namespaces.

Currently not finished.

## Documentation

The source code is documented using [doxygen](https://www.doxygen.nl/). Pass the `--enable-doc` option to `./configure` to generate all documentation. (For more details, see `./configure --help`)

## Versioning

This project uses [semantic versioning](https://semver.org).

This project is split into 3 parts: The specification rxc, once existing, the library librxc and the compiler frontend rxcc.

## License

For any copyright year range specified as YYYY-ZZZZ in this package
note that the range specifies every single year in that closed interval.

Copyright (C) 2021-2023 Dreche

rxcc and librxc is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

rxcc and librxc are distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with rxcc and librxc.  If not, see <http://www.gnu.org/licenses/>.

For more information, see the full license text: [COPYING](COPYING)

## Library Structure

The library uses several namespaces inside the `rxc` namespace:

| Namespace | Usage description                                    |
|----------:|------------------------------------------------------|
| `Source*` | Management of source files and positions within them |
| `cst`     | Concrete syntax tree items (along parsers)           |
| `parser`  | General purpose parser functions                     |
| `error`   | Error reporting (and errors themselves)              |
| `tokens`  | Token types                                          |

## Help needed

The CI/CD configuration is far from perfect and a try to improve it failed with a [cryptic template error message](https://gitlab.com/TheDreche/rxc/-/jobs/3084512497#L178).

If you have experience in such things and the time, this would help a lot. Otherwise, I will do it eventually.
