/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ERROR_HPP
#define RXC_ERROR_HPP

/*!
 * @file
 * @brief General errors.
 * @sa rxc::error
 */

// General error interface
#include "error/error_exception.hpp"
#include "error/error_general.hpp"
#include "error/error_general_description.hpp"
#include "error/error_general_hint.hpp"
#include "error/error_general_suggestion.hpp"
#include "error/error_level.hpp"
#include "error/error_levels.hpp"
#include "error/error_staged.hpp"
#include "error/error_staged_description.hpp"
#include "error/error_staged_hint.hpp"
#include "error/error_staged_suggestion.hpp"
#include "error/error_type.hpp"

// Specific error types
#include "error/general_parser_error.hpp"
#include "error/general_parser_exception.hpp"
#include "error/invalid_token_start_error.hpp"

namespace rxc {
	/*!
	 * @brief General errors.
	 *
	 * There is a naming scheme in this namespace:
	 * - If it starts with `Error`, it is part of the error interface:
	 * 	- If it starts with `ErrorStaged`, it is a template taking a type to save the position
	 * 	- If it starts with `ErrorGeneral`, it is an abstract superclass for the `ErrorStaged` types
	 * 	- Otherwise, it represents a component not dependent on the stage
	 * - If it doesn't start with `Error`, it is a specific diagnostic:
	 * 	- If it ends with `Error`, it is the actual diagnostic class
	 * 	- If it ends with `Exception`, it is a wrapper around the corresponding `Error`
	 */
	namespace error {}
}

#endif
