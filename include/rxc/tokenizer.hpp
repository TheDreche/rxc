/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_TOKENIZER_HPP
#define RXC_TOKENIZER_HPP

/*!
 * @file
 * @brief Lexer.
 * @sa rxc::tokenizer
 * @todo Rename to tokenizer.hpp
 */

#include "tokenizer/file.hpp"
#include "tokenizer/invalid.hpp"
#include "tokenizer/literal_colon.hpp"
#include "tokenizer/literal_comma.hpp"
#include "tokenizer/literal_comment_eol.hpp"
#include "tokenizer/literal_comment_multiline.hpp"
#include "tokenizer/literal_curly_bracket_left.hpp"
#include "tokenizer/literal_curly_bracket_right.hpp"
#include "tokenizer/literal_dot.hpp"
#include "tokenizer/literal_eof.hpp"
#include "tokenizer/literal_equals.hpp"
#include "tokenizer/literal_identifier.hpp"
#include "tokenizer/literal_parentheses_left.hpp"
#include "tokenizer/literal_parentheses_right.hpp"
#include "tokenizer/literal_semicolon.hpp"
#include "tokenizer/literal_whitespace.hpp"
#include "tokenizer/position.hpp"
#include "tokenizer/span.hpp"
#include "tokenizer/stage_description.hpp"
#include "tokenizer/token.hpp"
#include "tokenizer/token_lexer.hpp"

namespace rxc {
	/*!
	 * @brief RXC Lexer.
	 *
	 * The tokenizer considers every character to be part of a token and there being whitespace and comment tokens.
	 */
	namespace tokenizer {}
}

#endif
