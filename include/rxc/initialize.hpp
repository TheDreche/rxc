/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_INITIALIZE_HPP
#define RXC_INITIALIZE_HPP

/*!
 * @file
 * @brief Initialize librxc.
 * @sa rxc::initialize
 */

#include "rxc/export.hpp"

namespace rxc {
	/*!
	 * @brief Initialize librxc.
	 * 
	 * This function initializes librxc. It has to be called before using any other function. Can safely be called multiple times, but the first call mustn't be multithreaded.
	 */
	void LIBRXC_EXPORT initialize();
}

#endif
