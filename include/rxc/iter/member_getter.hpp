/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ITER_MEMBER_GETTER_HPP
#define RXC_ITER_MEMBER_GETTER_HPP

#include <cassert>
#include <cstddef>

/*!
 * @file
 * @brief Iteratable over return value of member function.
 * @sa rxc::iter::MemberGetter
 */

#include "../export.hpp"

namespace rxc::iter {
	/*!
	 * @brief Iteratable over return value of member function.
	 */
	template<class _ItemT, class _ContainterT>
	class MemberGetter final {
	public:
		using ContainerT = _ContainterT;
		using ItemT = _ItemT;
		using GetterT = const ItemT*(ContainerT::*)(std::size_t)const;

		class Iterator {
		private:
			std::size_t position;
			const MemberGetter<ItemT, ContainerT>* container;

		public:
			explicit Iterator(std::size_t position, const MemberGetter<ItemT, ContainerT>* container)
				: position(position)
				, container(container)
			{}

		public:
			[[nodiscard]] const ItemT& operator*() const {
				return *(container->container->*(this->container->getter)(this->position));
			}
			Iterator& operator++() {
				++this->position;
				return *this;
			}
			[[nodiscard]] bool operator!=(const Iterator& other) const {
				assert(this->container->container == other.container->container);
				return this->position != other.position;
			}
		};

	private:
		const ContainerT* container;
		const _ItemT*(_ContainterT::* getter)(std::size_t)const;

	public:
		explicit MemberGetter(const ContainerT* container, GetterT getter)
			: container(container)
			, getter(getter)
		{}

	public:
		[[nodiscard]] Iterator begin() const {
			return Iterator(0, this);
		}
		[[nodiscard]] Iterator end() const {
			std::size_t last = 0;
			while(this->container->*(this->getter)(last) != nullptr) {
				++last;
			}
			return Iterator(last, this);
		}
	};
}

#endif
