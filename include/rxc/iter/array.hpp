/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ITER_ARRAY_HPP
#define RXC_ITER_ARRAY_HPP

/*!
 * @file
 * @brief General iteratable object over arrays.
 * @sa rxc::iter::Array
 */

#include "../export.hpp"

namespace rxc::iter {
	/*!
	 * @brief General iteratable object over arrays.
	 */
	template<class _ItemT>
	class Array final {
	public:
		using ItemT = _ItemT;

	public:
		const ItemT* start;
		const ItemT* ending;

	public:
		Array()
			: start(nullptr)
			, ending(nullptr)
		{}

		explicit Array(const ItemT* start, const ItemT* ending)
			: start(start)
			, ending(ending)
		{}

	public:
		[[nodiscard]] const ItemT* begin() const {
			return this->start;
		}
		[[nodiscard]] const ItemT* end() const {
			return this->ending;
		}
	};
}

#endif
