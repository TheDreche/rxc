/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_NODE_WRAPPER_HPP
#define RXC_PARSER_NODE_WRAPPER_HPP

#include <cstddef>

/*!
 * @file
 * @brief Wrapper node abstraction.
 * @sa rxc::parser::NodeWrapper
 */

#include "../configuration.hpp"
#include "../lexer/position.hpp"

#include "node.hpp"
#include "parser_heap.hpp"
#include "stage_description.hpp"
#include "state.hpp"
#include "wrapper.hpp"

namespace rxc::parser {
	template<class Content, const ParserHeap<Content>* = nullptr>
	class NodeWrapper;

	namespace _internal::_parseWrapper {
		template<class ParserData>
		inline void parse(
			ParserData&,
			const Configuration&,
			State&
		);
	}

	template<class ResultT, class ParserData>
	[[nodiscard]] std::unique_ptr<ResultT> parseWrapper(const Configuration&, State&);

	template<class ResultT, class ParserData>
	[[nodiscard]] ResultT parseWrapperStack(const Configuration&, State&);

	/*!
	 * @brief Wrapper node.
	 *
	 * This is an abstraction for giving special arguments to the wrapper class, which is needed sometimes.
	 *
	 * @tparam Content The node type contained, same as in @ref Wrapper.
	 */
	template<class Content, const ParserHeap<Content>* _parser>
	class NodeWrapper : public Node {
	private:
		using NodeT = Content;
		using SelfT = NodeWrapper<NodeT, _parser>;
		using WrapperT = Wrapper<NodeT, _parser>;

	protected:
		/*!
		 * @brief Parser data.
		 *
		 * This exposes the node being saved. It, theoretically, could be named by
		 * itself, but that would either be code duplication (because of needing
		 * to name it again) or would introduce unnecessary symbols
		 * (one symbol for this type from outside the class).
		 *
		 * This type is needed because it can be used for automatically generating the
		 * parsing functions.
		 */
		using ParserData = SelfT;

	private:
		WrapperT content;

		friend void _internal::_parseWrapper::parse<ParserData>(SelfT&, const Configuration&, State&);

	public:
		NodeWrapper() = default;

		[[nodiscard]] static inline std::unique_ptr<SelfT> parse(const Configuration& config, State& state) {
			return parseWrapper<SelfT, ParserData>(config, state);
		}
		[[nodiscard]] static inline SelfT parseStack(const Configuration& config, State& state) {
			return parseWrapperStack<SelfT, ParserData>(config, state);
		}

	public:
		[[nodiscard]] const WrapperT& value() const {
			return this->content;
		}
		[[nodiscard]] WrapperT& value() {
			return this->content;
		}

	public:
		[[nodiscard]] virtual const Node* traverseNode(std::size_t& number) const final override {
			if(number == 0) {
				return this;
			}
			--number;
			return this->value().traverseNode(number);
		}
		[[nodiscard]] virtual const stage_description::Error* traverseError(std::size_t& number) const final override {
			return this->value().traverseError(number);
		}

		[[nodiscard]] virtual lexer::Position begin() const final override {
			return this->value().begin();
		}
		[[nodiscard]] virtual lexer::Position end() const final override {
			return this->value().end();
		}
	};

	namespace _internal::_parseWrapper {
		template<class ParserData>
		inline void parse(
			ParserData& node,
			const Configuration& config,
			State& state
		) {
			node.content.parse(config, state);
		}
	}

	/*!
	 * @brief Parse a wrapper node.
	 *
	 * @tparam ResultT The actual node type to parse, inheriting from @ref parser::NodeWrapper<class>.
	 * @tparam ParserData The parser data as exposed by @ref parser::NodeWrapper<class>
	 * @param config The configuration.
	 * @param state The parser state.
	 * @return The parsed result.
	 */
	template<class ResultT, class ParserData>
	[[nodiscard]] std::unique_ptr<ResultT> parseWrapper(const Configuration& config, State& state) {
		std::unique_ptr<ResultT> result = std::make_unique<ResultT>();
		_internal::_parseWrapper::parse<ParserData>(*result, config, state);
		return result;
	}

	/*!
	 * @brief Parse a wrapper node.
	 *
	 * This is the stack version.
	 *
	 * @tparam ResultT The actual node type to parse, inheriting from @ref parser::NodeWrapper<class>.
	 * @tparam ParserData The parser data as exposed by @ref parser::NodeWrapper<class>
	 * @param config The configuration.
	 * @param state The parser state.
	 * @return The parsed result.
	 */
	template<class ResultT, class ParserData>
	[[nodiscard]] ResultT parseWrapperStack(const Configuration& config, State& state) {
		auto result = ResultT();
		_internal::_parseWrapper::parse<ParserData>(result, config, state);
		return result;
	}
}

#endif
