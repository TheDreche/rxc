/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_PARSE_DECISION_HPP
#define RXC_PARSER_PARSE_DECISION_HPP

#include <cassert>
#include <memory>
#include <utility>
#include <variant>
#include <vector>

/*!
 * @file
 * @brief Parser decisions.
 * @sa rxc::parser::parseDecision
 */

#include "../configuration.hpp"

#include "../error/general_parser_error.hpp"
#include "../error/general_parser_exception.hpp"

#include "parser_heap.hpp"
#include "parser_postfix.hpp"
#include "state.hpp"

namespace rxc::parser {
	/*!
	 * @brief Parser decision.
	 *
	 * This is a function being able to parse nodes that can have multiple
	 * grammars for them, like an expression could be a function or a constant.
	 *
	 * There are two template parameters, both for the list of options: The
	 * postfix one is needed because this parser is a recursive descant parser.
	 * Since it usually has trouble with left recursion, the postfix parsers
	 * represent the options having left recursion separately.
	 *
	 * @tparam ResultT The superclass all options for the decision inherit from.
	 * @tparam configuration_option The list of functions in the configuration, each representing a single grammar rule.
	 * @tparam configuration_option_postfix The list for the postfix parsers.
	 * @param config The configuration.
	 * @param state The parser state.
	 * @return The parsed result, wrapped in std::unique_ptr.
	 */
	template<
		class ResultT,
		std::vector<ParserHeap<ResultT>> Configuration::* configuration_option,
		std::vector<ParserPostfix<ResultT>> Configuration::* configuration_option_postfix = nullptr
	> std::unique_ptr<ResultT> parseDecision(const Configuration& config, State& state) {
		// 1. Set up storage for the result
		struct Result {
			std::variant<std::unique_ptr<ResultT>, error::GeneralParserException> data;
			State state;

			inline bool has_error() const {
				return std::holds_alternative<error::GeneralParserException>(this->data);
			}

			void improve(Result&& other) {
				// This function checks which has the better error, this or other, and saves the best of them
				assert(this->has_error());
				assert(other.has_error());
				if(other.state.getPosition() > this->state.getPosition()) {
					*this = std::move(other);
				}
			}
		};
		auto result = Result {
			.data = error::GeneralParserException{
				.error = error::GeneralParserError(state.getFile(), state.getPosition()),
			},
			.state = state,
		};

		// 2. Find first result without error (or best result with error)
		for(const ParserHeap<ResultT>& parser : config.*configuration_option) {
			auto current_try = Result {
				.data = nullptr,
				.state = state
			};

			try {
				current_try.data = parser(config, current_try.state);
				// Found first result without error
				result = std::move(current_try);
				break;
			} catch(const error::GeneralParserException& error) {
				current_try.data = error;
				result.improve(std::move(current_try));
			}
		}

		// 3. When not having an error, try to expand the result using suffix parsers
		if constexpr(configuration_option_postfix != nullptr) {
			if(std::unique_ptr<ResultT>* object = std::get_if<std::unique_ptr<ResultT>>(&result.data)) {
				bool changed;
				do {
					changed = false;
					for(const ParserPostfix<ResultT>& parser : config.*configuration_option_postfix) {
						try {
							State position = result.state;
							parser(*object, config, position);
							result.state = position;

							// Always prefer the first items in the configuration.
							// For this, after succeeding one time the next try needs to start from the beginning of the list.
							changed = true;
							break;
						} catch(const error::GeneralParserException&) {}
					}
				} while(changed);
			}
		}

		// 4. Return the final result
		state = result.state;
		struct V {
			std::unique_ptr<ResultT> operator()(error::GeneralParserException& error) {
				throw error;
			}
			std::unique_ptr<ResultT> operator()(std::unique_ptr<ResultT>& result) {
				return std::move(result);
			}
		};
		return std::visit(V(), result.data);
	}
}

#endif
