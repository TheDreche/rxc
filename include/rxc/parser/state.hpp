/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_STATE_HPP
#define RXC_PARSER_STATE_HPP

#include <memory>
#include <vector>

/*!
 * @file
 * @brief Parser state.
 * @sa rxc::parser::State
 */

#include "../export.hpp"

#include "../lexer/file.hpp"
#include "../lexer/position.hpp"
#include "../lexer/token.hpp"

#include "../sources/span.hpp"

namespace rxc::parser {
	/*!
	 * @brief Parser state.
	 */
	class LIBRXC_EXPORT State final {
	private:
		lexer::Position position;
		const lexer::File* file;

	public:
		explicit State(const lexer::File*, const lexer::Position&);

	public:
		State& operator++();

		[[nodiscard]] bool operator==(const State&) const;
		[[nodiscard]] bool operator!=(const State&) const;

		[[nodiscard]] bool operator<=(const State&) const;
		[[nodiscard]] bool operator>=(const State&) const;
		[[nodiscard]] bool operator<(const State&) const;
		[[nodiscard]] bool operator>(const State&) const;

		[[nodiscard]] const lexer::Token& operator*() const;
		[[nodiscard]] const lexer::Token* operator->() const;

	public:
		template<class TokenT>
		[[nodiscard]] bool isAt() const {
			return (*this)->isType<TokenT>();
		}

	public:
		[[nodiscard]] const lexer::Position& getPosition() const;
		[[nodiscard]] const lexer::File* getFile() const;
	};
}

#endif
