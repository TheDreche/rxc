/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_NODE_DECISION_CRTP_HPP
#define RXC_PARSER_NODE_DECISION_CRTP_HPP

#include <memory>

/*!
 * @file
 * @brief Decision node abstraction.
 * @sa rxc::parser::NodeDecisionCrtp
 */

#include "../configuration.hpp"

#include "parse_decision.hpp"
#include "state.hpp"

namespace rxc::parser {
	/*!
	 * @brief CRTP decision node wrapper.
	 *
	 * This makes it easier to set up a working node by doing
	 * the work of defining the static parsing functions.
	 *
	 * With this, your class could look like this:
	 *
	 * 	class MyNode : public NodeDecisionCrtp<MyNode, Node1, Node2> {};
	 *
	 * This is special in the field of CRTP implementations as
	 * this is for decision nodes and they are special because
	 * they don't inherit from Node because then it would be hard
	 * to avoid inheriting from it multiple times.
	 *
	 * Previously, this had a `CRTP` using declaration inside it,
	 * which turned out to make it hard to inherit from this and
	 * use another CRTP implementation (as done in all nodes
	 * implementing a decision node) because then the `CRTP` type
	 * was ambiguous for C++ (because name resolution comes before
	 * accessability checking and needs to yield a unique name).
	 * Because of that, this got removed and for not needing to
	 * name this CRTP implementation class twice, it has to be
	 * saved using a `using` declaraion in an internal namespace.
	 *
	 * @tparam NodeT The class to be defined using this.
	 * @tparam configuration_option The parser configuration option as pointer to member of Configuration.
	 * @tparam configuration_option_postfix The postfix parser configuration option.
	 */
	template<
		class NodeT, 
		std::vector<ParserHeap<NodeT>> Configuration::* configuration_option,
		std::vector<ParserPostfix<NodeT>> Configuration::* configuration_option_postfix = nullptr
	>
	class NodeDecisionCrtp {
	public:
		virtual ~NodeDecisionCrtp() = default;

	public:
		[[nodiscard]] static inline std::unique_ptr<NodeT> parse(const Configuration& config, State& state) {
			return parseDecision<NodeT, configuration_option, configuration_option_postfix>(config, state);
		}
	};
}

#endif
