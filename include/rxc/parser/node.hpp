/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_FILE_NODE_HPP
#define RXC_PARSER_FILE_NODE_HPP

/*!
 * @file
 * @brief CST file node.
 * @sa rxc::parser::FileNode
 */

#include "../export.hpp"

#include "../error/error_staged.hpp"
#include "../stage/item.hpp"

#include "../lexer/position.hpp"
#include "../lexer/span.hpp"

#include "stage_description.hpp"

namespace rxc::parser {
	/*!
	 * @brief CST file node.
	 */
	class LIBRXC_EXPORT Node : public stage::Item {
	public:
		Node();
		virtual ~Node();

	public:
		[[nodiscard]] virtual const Node* traverseNode(std::size_t&) const = 0;
		[[nodiscard]] virtual const stage_description::Error* traverseError(std::size_t&) const override = 0;

	public:
		[[nodiscard]] virtual lexer::Position begin() const = 0;
		[[nodiscard]] virtual lexer::Position end() const = 0;

	public:
		[[nodiscard]] lexer::Span span() const;
	};
}

#endif
