/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_FILE_HPP
#define RXC_PARSER_FILE_HPP

#include <cstddef>
#include <variant>

/*!
 * @file
 * @brief Parsed file.
 * @sa rxc::parser::File
 */

#include "../export.hpp"

#include "../configuration.hpp"
#include "../cst/block_content.hpp"
#include "../error/general_parser_error.hpp"
#include "../lexer/file.hpp"
#include "../stage/result.hpp"

#include "node.hpp"
#include "position.hpp"
#include "stage_description.hpp"

namespace rxc::parser {
	/*!
	 * @brief Parsed file.
	 *
	 * When constructing one, one needs a @ref SourceFile. It will be parsed automatically.
	 *
	 * @todo Add separate unexpected EOF error.
	 */
	class LIBRXC_EXPORT File final : public stage::Result {
	private:
		using ErrorVariant = std::variant<
			std::monostate,
			error::GeneralParserError
		>;

	public:
		lexer::File file;
		cst::BlockContent content;

	private:
		ErrorVariant error;

	public:
		explicit File(lexer::File&& file, const Configuration& config);

		[[nodiscard]] const Node* dereference(const Position&) const;
		[[nodiscard]] virtual const stage_description::Error* getError(std::size_t) const override;
	};
}

#endif
