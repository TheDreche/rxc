/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_NODE_COMPOUND_HPP
#define RXC_PARSER_NODE_COMPOUND_HPP

#include <cassert>
#include <cstddef>
#include <memory>
#include <type_traits>
#include <utility>

/*!
 * @file
 * @brief Compound node abstraction.
 * @sa rxc::parser::NodeCompound
 */

#include "../configuration.hpp"
#include "../lexer/position.hpp"

#include "node.hpp"
#include "stage_description.hpp"
#include "state.hpp"
#include "wrapper.hpp"

namespace rxc::parser {
	namespace _internal::_parseCompound {
		// These functions also serve for parseCompoundStack

		template<class ParserData>
		inline void parseCompound(ParserData&, const Configuration&, State&);

		template<class DataT>
		inline void parseRemainder(DataT&, const Configuration&, State&);
	}

	template<class...>
	class NodeCompound;

	template<class ResultT, class ParserData>
	[[nodiscard]] std::unique_ptr<ResultT> parseCompound(const Configuration&, State&);

	template<class ResultT, class ParserData>
	[[nodiscard]] ResultT parseCompoundStack(const Configuration&, State&);

	template<class ResultT, class NodeT, class ParserData>
	void parseCompoundPostfix(std::unique_ptr<ResultT>&, const Configuration&, State&);

	namespace _internal {
		template<class...>
		class _NodeCompound;

		template<class _NodeT, class... _Remainder>
		class _NodeCompound<_NodeT, _Remainder...> final {
		private:
			using NodeT = _NodeT;
			using SelfT = _NodeCompound<_NodeT, _Remainder...>;

		private:
			Wrapper<NodeT> node;
			_NodeCompound<_Remainder...> remainder;

			friend void _internal::_parseCompound::parseRemainder<SelfT>(SelfT&, const Configuration&, State&);
			template<class ResultT, class NodeT, class ParserData>
			friend void parser::parseCompoundPostfix(std::unique_ptr<ResultT>&, const Configuration&, State&);

			static constexpr inline bool has_successor = true;

		public:
			_NodeCompound() = default;

			template<std::size_t number>
			[[nodiscard]] inline constexpr const auto& get() const {
				if constexpr(number == 0) {
					return this->node;
				} else {
					return this->remainder.template get<number - 1>();
				}
			}

			[[nodiscard]] inline const Node* traverseNode(std::size_t& number) const {
				if(const Node* result = this->node.traverseNode(number)) {
					return result;
				}
				return this->remainder.traverseNode(number);
			}
			[[nodiscard]] inline const stage_description::Error* traverseError(std::size_t& number) const {
				if(const stage_description::Error* result = this->node.traverseError(number)) {
					return result;
				}
				return this->remainder.traverseError(number);
			}

			[[nodiscard]] inline lexer::Position begin() const {
				return this->node.begin();
			}
			[[nodiscard]] inline lexer::Position end() const {
				return this->remainder.end();
			}
		};

		template<class _NodeT>
		class _NodeCompound<_NodeT> final {
		private:
			using NodeT = _NodeT;
			using SelfT = _NodeCompound<_NodeT>;

		private:
			Wrapper<NodeT> node;

			friend void _internal::_parseCompound::parseRemainder<SelfT>(SelfT&, const Configuration&, State&);
			template<class ResultT, class NodeT, class ParserData>
			friend void parser::parseCompoundPostfix(std::unique_ptr<ResultT>&, const Configuration&, State&);

			static constexpr inline bool has_successor = false;

		public:
			_NodeCompound() = default;

			template<std::size_t number>
			[[nodiscard]] inline constexpr const auto& get() const {
				static_assert(number == 0, "Subnode index for NodeCompound out of bounds");
				return this->node;
			}

			[[nodiscard]] inline const Node* traverseNode(std::size_t& number) const {
				return this->node.traverseNode(number);
			}
			[[nodiscard]] inline const stage_description::Error* traverseError(std::size_t& number) const {
				return this->node.traverseError(number);
			}

			[[nodiscard]] inline lexer::Position begin() const {
				return this->node.begin();
			}
			[[nodiscard]] inline lexer::Position end() const {
				return this->node.end();
			}
		};
	}

	/*!
	 * @brief Compound node.
	 *
	 * This is an abstraction for implementing all nodes being composed from other nodes.
	 *
	 * @tparam Content The node types contained.
	 */
	template<class... Content>
	class NodeCompound : public Node {
	private:
		using SelfT = NodeCompound<Content...>;

	protected:
		/*!
		 * @brief Compound class.
		 *
		 * This exposes the NodeCompound instanciation to the node. It, theoretically, could name it by itself, but that would either be code duplication (because of needing to name all members again) or would introduce unnecessary symbols (one symbol for this type from outside the class).
		 *
		 * This type is needed because it can be used for automatically generating the parsing functions.
		 *
		 * So, your class body should contain code like this:
		 * 
		 * 	static std::unique_ptr<YourNode> parse(const rxc::Configuration& config, rxc::parser::State& state) {
		 * 		return rxc::parser::parseCompound<YourNode, ParserData>(config, state);
		 * 	}
		 * 	static YourNode parseStack(const rxc::Configuration& config, rxc::parser::State& state) {
		 * 		return rxc::parser::parseCompoundStack<YourNode, ParserData>(config, state);
		 * 	}
		 *
		 * You may consider making the functions inline for optimizing this one function call if possible.
		 */
		using ParserData = SelfT;

	private:
		_internal::_NodeCompound<Content...> data;

		template<class ParserData>
		friend void _internal::_parseCompound::parseCompound(ParserData&, const Configuration&, State&);
		template<class ResultT, class NodeT, class ParserData>
		friend void parseCompoundPostfix(std::unique_ptr<ResultT>&, const Configuration&, State&);

	public:
		NodeCompound() = default;

		[[nodiscard]] static inline std::unique_ptr<SelfT> parse(const Configuration& config, State& state) {
			return parseCompound<SelfT, ParserData>(config, state);
		}
		[[nodiscard]] static inline SelfT parseStack(const Configuration& config, State& state) {
			return parseCompoundStack<SelfT, ParserData>(config, state);
		}

	public:
		/*!
		 * @brief Get a specific subnode.
		 *
		 * This is the way to access the individual components.
		 *
		 * @return Constant Wrapper<...> reference (const Wrapper<NodeT>&) to the actual node.
		 */
		template<std::size_t number>
		[[nodiscard]] inline constexpr const auto& subnode() const {
			return this->data.template get<number>();
		}

	public:
		[[nodiscard]] virtual const Node* traverseNode(std::size_t& number) const final override {
			if(number == 0) {
				return this;
			}
			--number;
			return this->data.traverseNode(number);
		}
		[[nodiscard]] virtual const stage_description::Error* traverseError(std::size_t& number) const final override {
			return this->data.traverseError(number);
		}

		[[nodiscard]] virtual lexer::Position begin() const final override {
			return this->data.begin();
		}
		[[nodiscard]] virtual lexer::Position end() const final override {
			return this->data.end();
		}
	};

	namespace _internal::_parseCompound {
		template<class ParserData>
		inline void parseCompound(ParserData& result, const Configuration& config, State& state) {
			// ParserData is the actual NodeCompound instanciation
			parseRemainder(result.data, config, state);
		}

		template<class DataT>
		inline void parseRemainder(DataT& data, const Configuration& config, State& state) {
			data.node.parse(config, state);
			if constexpr(DataT::has_successor) {
				parseRemainder(data.remainder, config, state);
			}
		}
	}

	/*!
	 * @brief Parse a compound node.
	 *
	 * @tparam ResultT The actual node type, should publicly and non-virtually inherit from the compound node (so that a static_cast works).
	 * @tparam ParserData The parser data as exposed by the compound node.
	 * @param config The configuration.
	 * @param state The current parsing state.
	 */
	template<class ResultT, class ParserData>
	[[nodiscard]] std::unique_ptr<ResultT> parseCompound(const Configuration& config, State& state) {
		std::unique_ptr<ResultT> result = std::make_unique<ResultT>();
		_internal::_parseCompound::parseCompound<ParserData>(*result, config, state);
		return result;
	}

	/*!
	 * @brief Parse a compound node.
	 *
	 * This is the stack version, directly returning the result instead of wrapping it inside a std::unique_ptr.
	 *
	 * @tparam ResultT The actual node type, should publicly and non-virtually inherit from the compound node (so that a static_cast works).
	 * @tparam ParserData The parser data as exposed by the compound node.
	 * @param config The configuration.
	 * @param state The current parsing state.
	 */
	template<class ResultT, class ParserData>
	[[nodiscard]] ResultT parseCompoundStack(const Configuration& config, State& state) {
		auto result = ResultT();
		_internal::_parseCompound::parseCompound<ParserData>(result, config, state);
		return result;
	}

	/*!
	 * @brief Parse a compound node.
	 *
	 * This is the postfix version, meaning the first item
	 * and the node to be parsed have a common superclass
	 * and this first item already has been parsed and
	 * should be tried to be extended.
	 *
	 * When parsing fails, will not catch the
	 * @ref error::GeneralParserException, but will make
	 * sure first is in the state it was before.
	 *
	 * @tparam ResultT The common superclass (like Expression).
	 * @tparam NodeT The compound node to actually parse.
	 * @tparam ParserData The parser data as exposed by the compound node.
	 * @param first The already parsed first node.
	 * @param config The configuration.
	 * @param state The parser state (after parsing first).
	 * @throw error::GeneralParserException When the next tokens don't match.
	 */
	template<class ResultT, class NodeT, class ParserData>
	void parseCompoundPostfix(std::unique_ptr<ResultT>& first, const Configuration& config, State& state) {
		static_assert(
			decltype(
				std::declval<ParserData>().data
			)::has_successor,
			"Compound nodes having a postfix parser need at least two elements"
		);
		std::unique_ptr<NodeT> result = std::make_unique<NodeT>();
		// ParserData is the actual NodeCompound instanciation
		_internal::_parseCompound::parseRemainder(static_cast<ParserData&>(*result).data.remainder, config, state);
		// Do this last as the previous parts may throw exceptions
		result->data.node = std::move(first);
		first = std::move(result);
	}
}

#endif
