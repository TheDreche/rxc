/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_PARSE_POSTFIX_HPP
#define RXC_PARSER_PARSE_POSTFIX_HPP

#include <memory>
#include <vector>

/*!
 * @file
 * @brief Parse postfix components.
 * @sa rxc::parser::parsePostfix
 */

#include "../configuration.hpp"
#include "../error/general_parser_exception.hpp"

#include "parser_postfix.hpp"
#include "state.hpp"

namespace rxc::parser {
	/*!
	 * @brief Parse postfix components.
	 *
	 * @tparam NodeT The CST node type to parse postfix additions to.
	 * @tparam postfix_parser_configuration_option Configuration property to look for postfix parsers.
	 * @param result The CST node to replace by the version with postfix parsed nodes.
	 * @param config Configuration.
	 * @param state Parser state.
	 * @param errors Error collector.
	 */
	template<
		class NodeT,
		const std::vector<ParserPostfix<NodeT>> Configuration::* postfix_parser_configuration_option
	>
	void parsePostfix(
		std::unique_ptr<const NodeT>& result,
		const Configuration& config,
		State& state
	) {
		if constexpr (postfix_parser_configuration_option != nullptr) {
			bool doneSomething;
			do {
				doneSomething = false;
				for (const ParserPostfix<const NodeT>& parser : config.*postfix_parser_configuration_option) {
					State pos_check = state;
					try {
						parser(result, config, pos_check);
						assert(result != nullptr);
					} catch (error::GeneralParserException) {
						continue;
					}
					doneSomething = true;
					state = pos_check;
					break;
				}
			} while (doneSomething);
		}
	}
}

#endif
