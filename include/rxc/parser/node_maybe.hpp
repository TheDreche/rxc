/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_NODE_MAYBE_HPP
#define RXC_PARSER_NODE_MAYBE_HPP

#include <cstddef>
#include <memory>
#include <variant>

/*!
 * @file
 * @brief Maybe node abstraction.
 * @sa rxc::parser::NodeMaybe
 */

#include "../configuration.hpp"
#include "../error/general_parser_exception.hpp"
#include "../lexer/position.hpp"

#include "node.hpp"
#include "stage_description.hpp"
#include "state.hpp"
#include "wrapper.hpp"

namespace rxc::parser {
	template<class>
	class NodeMaybe;

	namespace _internal::_parseMaybe {
		template<class ParserData>
		inline void parse(
			NodeMaybe<ParserData>&,
			const Configuration&,
			State&
		);
	}

	template<class ResultT, class ParserData>
	[[nodiscard]] std::unique_ptr<ResultT> parseMaybe(const Configuration&, State&);

	template<class ResultT, class ParserData>
	[[nodiscard]] ResultT parseMaybeStack(const Configuration&, State&);

	/*!
	 * @brief Maybe node.
	 *
	 * This is an abstraction for implementing all nodes maybe containing another.
	 *
	 * @tparam Content The node type possibly contained.
	 */
	template<class Content>
	class NodeMaybe : public Node {
	private:
		using NodeT = Content;
		using SelfT = NodeMaybe<NodeT>;

	protected:
		/*!
		 * @brief Maybe class.
		 *
		 * This exposes the node possibly being saved to the node. It, theoretically,
		 * could name it by itself, but that would either be code duplication
		 * (because of needing to name it again) or would introduce unnecessary symbols
		 * (one symbol for this type from outside the class).
		 *
		 * This type is needed because it can be used for automatically generating the
		 * parsing functions.
		 */
		using ParserData = NodeT;

	private:
		std::variant<lexer::Position, Wrapper<NodeT>> content;

		friend void _internal::_parseMaybe::parse<NodeT>(SelfT&, const Configuration&, State&);

	public:
		NodeMaybe() = default;

		[[nodiscard]] static inline std::unique_ptr<SelfT> parse(const Configuration& config, State& state) {
			return parseMaybe<SelfT, ParserData>(config, state);
		}
		[[nodiscard]] static inline SelfT parseStack(const Configuration& config, State& state) {
			return parseMaybeStack<SelfT, ParserData>(config, state);
		}

	public:
		/*!
		 * @copybrief pointer()const
		 * @copydoc pointer()const
		 */
		[[nodiscard]] inline Wrapper<NodeT>* pointer() {
			struct T {
				Wrapper<NodeT>* operator()(lexer::Position&) {
					return nullptr;
				}
				Wrapper<NodeT>* operator()(Wrapper<NodeT>& node) {
					return &node;
				}
			};
			return std::visit(T(), this->content);
		}

		/*!
		 * @brief Access content.
		 *
		 * This checks whether this node has content.
		 * If it has, returns a pointer to the wrapper (const Wrapper<NodeT>*).
		 * If not, returns nullptr.
		 *
		 * This is meant to be used in a context like this:
		 *
		 * 	if(const Wrapper<NodeT>* content = this->pointer()) {
		 * 		// Has content, accessible through *content
		 * 	} else {
		 * 		// Has no content
		 * 	}
		 *
		 * @return Pointer to wrapper to content (const Wrapper<NodeT>*), or nullptr if not available.
		 */
		[[nodiscard]] inline const Wrapper<NodeT>* pointer() const {
			struct T {
				const Wrapper<NodeT>* operator()(const lexer::Position&) {
					return nullptr;
				}
				const Wrapper<NodeT>* operator()(const Wrapper<NodeT>& node) {
					return &node;
				}
			};
			return std::visit(T(), this->content);
		}

		/*!
		 * @brief Check for existance of content.
		 *
		 * This function is there for simplifying use of @ref pointer()const when not caring for the actual content.
		 *
		 * @return true if this node has content, false otherwise.
		 */
		[[nodiscard]] inline bool hasContent() const {
			return this->pointer() != nullptr;
		}

	public:
		[[nodiscard]] virtual const Node* traverseNode(std::size_t& number) const final override {
			if(number == 0) {
				return this;
			}
			--number;
			if(const Wrapper<NodeT>* content = this->pointer()) {
				return content->traverseNode(number);
			} else {
				return nullptr;
			}
		}
		[[nodiscard]] virtual const stage_description::Error* traverseError(std::size_t& number) const final override {
			if(const Wrapper<NodeT>* content = this->pointer()) {
				return content->traverseError(number);
			} else {
				return nullptr;
			}
		}

		[[nodiscard]] virtual lexer::Position begin() const final override {
			struct T {
				lexer::Position operator()(const Wrapper<NodeT>& node) {
					return node.begin();
				}
				lexer::Position operator()(const lexer::Position& position) {
					return position;
				}
			};
			return std::visit(T(), this->content);
		}
		[[nodiscard]] virtual lexer::Position end() const final override {
			struct T {
				lexer::Position operator()(const Wrapper<NodeT>& node) {
					return node.end();
				}
				lexer::Position operator()(const lexer::Position& position) {
					return position;
				}
			};
			return std::visit(T(), this->content);
		}
	};

	namespace _internal::_parseMaybe {
		template<class ParserData>
		inline void parse(
			NodeMaybe<ParserData>& node_maybe,
			const Configuration& config,
			State& state
		) {
			// ParserData is the node type maybe being parsed
			try {
				State position = state;
				auto node = Wrapper<ParserData>();

				node.parse(config, position);

				node_maybe.content = std::move(node);
				state = position;
			} catch (const error::GeneralParserException&) {
				node_maybe.content = state.getPosition();
			}
		}
	}

	/*!
	 * @brief Maybe parse a node.
	 *
	 * @tparam ResultT The actual node type to parse, inheriting from @ref parser::NodeMaybe<class>.
	 * @tparam ParserData The parser data as exposed by @ref parser::NodeMaybe<class>
	 * @param config The configuration.
	 * @param state The parser state.
	 * @return The parsed result.
	 */
	template<class ResultT, class ParserData>
	[[nodiscard]] std::unique_ptr<ResultT> parseMaybe(const Configuration& config, State& state) {
		std::unique_ptr<ResultT> result = std::make_unique<ResultT>();
		_internal::_parseMaybe::parse<ParserData>(*result, config, state);
		return result;
	}

	/*!
	 * @brief Maybe parse a node.
	 *
	 * This is the stack version.
	 *
	 * @tparam ResultT The actual node type to parse, inheriting from @ref parser::NodeMaybe<class>.
	 * @tparam ParserData The parser data as exposed by @ref parser::NodeMaybe<class>
	 * @param config The configuration.
	 * @param state The parser state.
	 * @return The parsed result.
	 */
	template<class ResultT, class ParserData>
	[[nodiscard]] ResultT parseMaybeStack(const Configuration& config, State& state) {
		auto result = ResultT();
		_internal::_parseMaybe::parse<ParserData>(result, config, state);
		return result;
	}
}

#endif
