/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_PARSER_STACK_HPP
#define RXC_PARSER_PARSER_STACK_HPP

#include <functional>

/*!
 * @file
 * @brief Stack parser.
 * @sa rxc::parser::ParserStack
 */

#include "state.hpp"

namespace rxc {
	struct Configuration;
}

namespace rxc::parser {
	/*!
	 * @brief Stack parser.
	 *
	 * This is called like this because it is used for saving it
	 * directly as member of an object, which usually would be on the
	 * stack, except when the object with the member is on the heap
	 * already.
	 */
	template<class ResultT>
	using ParserStack = std::function<ResultT(const Configuration&, State&)>;
}

#endif
