/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_WRAPPER_HPP
#define RXC_PARSER_WRAPPER_HPP

#include <cassert>
#include <cstddef>
#include <memory>
#include <type_traits>
#include <utility>

/*!
 * @file
 * @brief General CST node wrapper.
 * @sa rxc::parser::Wrapper<NodeT>
 */

#include "../configuration.hpp"

#include "node.hpp"
#include "parser_heap.hpp"
#include "stage_description.hpp"
#include "state.hpp"

namespace rxc::parser {
	namespace _internal::_Wrapper {
		template<class _T, std::size_t = 0>
		struct isDefined {
			static constexpr const bool value = false;
		};
		template<class _T>
		struct isDefined<_T, sizeof(_T)> {
			static constexpr const bool value = true;
		};

		template<class _NodeT, const ParserHeap<_NodeT>* _parser = nullptr>
		class WrapperHeap final {
		public:
			using NodeT = _NodeT;
		private:
			using This = WrapperHeap<NodeT>;

		private:
			std::unique_ptr<const NodeT> node = nullptr;

			[[nodiscard]] const Node& value_node() const {
				return dynamic_cast<const Node&>(*this->node);
			}

		public:
			WrapperHeap() = default;

			explicit WrapperHeap(std::unique_ptr<const NodeT>&& node)
				: node(std::move(node))
			{
				assert(this->node != nullptr);
			}

			This& operator=(std::unique_ptr<const NodeT>&& node) {
				this->node = std::move(node);
				assert(this->node != nullptr);
				return *this;
			}

			void parse(const Configuration& config, State& state) {
				assert(this->node == nullptr);
				if constexpr(_parser != nullptr) {
					this->node = (*_parser)(config, state);
				} else {
					this->node = NodeT::parse(config, state);
				}
				assert(this->node != nullptr);
			}

			[[nodiscard]] const Node* traverseNode(std::size_t& number) const {
				return this->value_node().traverseNode(number);
			}
			[[nodiscard]] const stage_description::Error* traverseError(std::size_t& result_error_number) const {
				return this->value_node().traverseError(result_error_number);
			}

			[[nodiscard]] lexer::Position begin() const {
				return this->value_node().begin();
			}
			[[nodiscard]] lexer::Position end() const {
				return this->value_node().end();
			}

			[[nodiscard]] NodeT& value() {
				assert(this->node != nullptr);
				return *this->node;
			}
			[[nodiscard]] const NodeT& value() const {
				assert(this->node != nullptr);
				return *this->node;
			}
			[[nodiscard]] NodeT* pointer() {
				return this->node;
			}
			[[nodiscard]] const NodeT pointer() const {
				return this->node;
			}

			[[nodiscard]] NodeT& operator*() {
				return this->value();
			}
			[[nodiscard]] const NodeT& operator*() const {
				return this->value();
			}
			[[nodiscard]] NodeT* operator->() {
				return &this->value();
			}
			[[nodiscard]] const NodeT* operator->() const {
				return &this->value();
			}
		};

		template<class _NodeT>
		class WrapperStack final {
		public:
			using NodeT = _NodeT;
		private:
			using This = WrapperStack<NodeT>;

		private:
			NodeT node;

		public:
			WrapperStack() = default;

			explicit WrapperStack(std::unique_ptr<NodeT>&& node)
				: node(std::move(*node))
			{}

			This& operator=(std::unique_ptr<NodeT>&& node) {
				this->node = std::move(*node);
				return *this;
			}

			void parse(const Configuration& config, State& state) {
				this->node = std::move(NodeT::parseStack(config, state));
			}

			[[nodiscard]] const NodeT* traverseNode(std::size_t& number) const {
				return this->value().traverseNode(number);
			}
			[[nodiscard]] const stage_description::Error* traverseError(std::size_t& result_error_number) const {
				return this->value().traverseError(result_error_number);
			}

			[[nodiscard]] lexer::Position begin() const {
				return this->value().begin();
			}
			[[nodiscard]] lexer::Position end() const {
				return this->value().end();
			}

			[[nodiscard]] NodeT& value() {
				return this->node;
			}
			[[nodiscard]] const NodeT& value() const {
				return this->node;
			}
			[[nodiscard]] NodeT* pointer() {
				return &this->node;
			}
			[[nodiscard]] const NodeT* pointer() const {
				return &this->node;
			}

			[[nodiscard]] NodeT& operator*() {
				return this->value();
			}
			[[nodiscard]] const NodeT& operator*() const {
				return this->value();
			}
			[[nodiscard]] NodeT* operator->() {
				return &this->value();
			}
			[[nodiscard]] const NodeT* operator->() const {
				return &this->value();
			}
		};
	}

	/*!
	 * @brief General CST node wrapper.
	 *
	 * @tparam NodeT The node to wrap.
	 * @tparam parser The (heap) parser function.
	 */
	template<class NodeT, const ParserHeap<NodeT>* parser = nullptr>
	using Wrapper = typename std::conditional<
		std::conditional<
			_internal::_Wrapper::isDefined<NodeT>::value,
			std::is_final<NodeT>,
			std::false_type
		>::type::value
		&& parser == nullptr,
		_internal::_Wrapper::WrapperStack<NodeT>,
		_internal::_Wrapper::WrapperHeap<NodeT, parser>
	>::type;
}

#endif
