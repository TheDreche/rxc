/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_NODE_TOKEN_HPP
#define RXC_PARSER_NODE_TOKEN_HPP

/*!
 * @file
 * @brief Token node abstraction.
 * @sa rxc::parser::NodeToken
 */

#include "../configuration.hpp"
#include "../error/general_parser_exception.hpp"
#include "../lexer/position.hpp"

#include "node.hpp"
#include "stage_description.hpp"
#include "state.hpp"
#include "wrapper.hpp"

namespace rxc::parser {
	namespace _internal::_parseToken {
		// These functions are also being used for parseTokenStack
		template<class ParserData>
		void parseToken(ParserData&, const Configuration&, State&);
	}

	template<class ResultT, class ParserData>
	[[nodiscard]] std::unique_ptr<ResultT> parseToken(const Configuration&, State&);

	template<class ResultT, class ParserData>
	[[nodiscard]] ResultT parseTokenStack(const Configuration&, State&);

	/*!
	 * @brief Token node.
	 *
	 * This is an abstraction for implementing all nodes that are a simple token as given by the lexer.
	 *
	 * @tparam TokenT The token type.
	 */
	template<class _TokenT>
	class NodeToken : public Node {
	private:
		template<class ParserData>
		friend void _internal::_parseToken::parseToken(ParserData&, const Configuration&, State&);

		using TokenT = _TokenT;
		using SelfT = NodeToken<TokenT>;
	public:
		using ParserData = SelfT;

	private:
		lexer::Position token_position;

	public:
		NodeToken() = default;

		[[nodiscard]] static inline std::unique_ptr<SelfT> parse(const Configuration& config, State& state) {
			return parseToken<SelfT, ParserData>(config, state);
		}
		[[nodiscard]] static inline SelfT parseStack(const Configuration& config, State& state) {
			return parseTokenStack<SelfT, ParserData>(config, state);
		}

	public:
		[[nodiscard]] virtual const Node* traverseNode(std::size_t& number) const final override {
			if(number == 0) {
				return this;
			}
			--number;
			return nullptr;
		}
		[[nodiscard]] virtual const stage_description::Error* traverseError(std::size_t&) const final override {
			return nullptr;
		}

		[[nodiscard]] virtual lexer::Position begin() const final override {
			return this->token_position;
		}
		[[nodiscard]] virtual lexer::Position end() const final override {
			return this->token_position;
		}
	};

	namespace _internal::_parseToken {
		template<class ParserData>
		void parseToken(ParserData& node, const Configuration&, State& state) {
			// ParserData is the NodeToken instanciation
			if(!state.isAt<typename ParserData::TokenT>()) {
				throw error::GeneralParserException {
					.error = error::GeneralParserError(
						state.getFile(),
						state.getPosition()
					)
				};
			}
			node.token_position = state.getPosition();
			++state;
		}
	}

	/*!
	 * @brief Parse token nodes.
	 *
	 * @tparam ResultT The actual node to parse.
	 * @tparam ParserData The parser data as exposed by the token node.
	 * @param config The configuration.
	 * @param state The parser state.
	 */
	template<class ResultT, class ParserData>
	[[nodiscard]] std::unique_ptr<ResultT> parseToken(const Configuration& config, State& state) {
		std::unique_ptr<ResultT> result = std::make_unique<ResultT>();
		_internal::_parseToken::parseToken<ParserData>(*result, config, state);
		return result;
	}

	/*!
	 * @brief Parse token nodes.
	 *
	 * This is the stack parser version.
	 *
	 * @tparam ResultT The actual node to parse.
	 * @tparam ParserData The parser data as exposed by the token node.
	 * @param config The configuration.
	 * @param state The parser state.
	 */
	template<class ResultT, class ParserData>
	[[nodiscard]] ResultT parseTokenStack(const Configuration& config, State& state) {
		auto result = ResultT();
		_internal::_parseToken::parseToken<ParserData>(result, config, state);
		return result;
	}
}

#endif
