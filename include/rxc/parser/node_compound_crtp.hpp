/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_NODE_COMPOUND_CRTP_HPP
#define RXC_PARSER_NODE_COMPOUND_CRTP_HPP

#include <memory>

/*!
 * @file
 * @brief CRTP compound node wrapper.
 * @sa rxc::parser::NodeCompoundCrtp
 */

#include "../configuration.hpp"

#include "node_compound.hpp"
#include "parse_compound.hpp"
#include "parse_compound_postfix.hpp"
#include "parse_compound_stack.hpp"
#include "state.hpp"

namespace rxc::parser {
	/*!
	 * @brief CRTP compound node wrapper.
	 *
	 * This makes it easier to set up a working node by doing
	 * the work of defining the static parsing functions.
	 *
	 * With this, your class could look like this:
	 *
	 * 	class MyNode : public NodeCompoundCrtp<MyNode, Node1, Node2> {};
	 *
	 * @tparam NodeT The class to be defined using this.
	 * @tparam Content The content, same as in @ref NodeCompound.
	 */
	template<class NodeT, class... Content>
	class NodeCompoundCrtp : public NodeCompound<Content...> {
	private:
		using Wrapped = NodeCompound<Content...>;
		using typename Wrapped::ParserData;

	protected:
		/*!
		 * @brief CRTP data.
		 *
		 * This is this class, implementing the static methods. Having this type alias makes
		 * it easier to use by not having to rewrite the type in the using declarations.
		 */
		using CRTP = NodeCompoundCrtp<NodeT, Content...>;

		using Wrapped::subnode;

	public:
		[[nodiscard]] static inline std::unique_ptr<NodeT> parse(const Configuration& config, State& state) {
			return parseCompound<NodeT, ParserData>(config, state);
		}
		[[nodiscard]] static inline NodeT parseStack(const Configuration& config, State& state) {
			return parseCompoundStack<NodeT, ParserData>(config, state);
		}
		template<class Superclass>
		static inline void parsePostfix(std::unique_ptr<Superclass>& result, const Configuration& config, State& state) {
			return parseCompoundPostfix<Superclass, NodeT, ParserData>(result, config, state);
		}
	};
}

#endif
