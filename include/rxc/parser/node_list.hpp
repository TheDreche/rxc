/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_NODE_LIST_HPP
#define RXC_PARSER_NODE_LIST_HPP

#include <cstddef>
#include <memory>
#include <optional>
#include <utility>

/*!
 * @file
 * @brief List node abstraction.
 * @sa rxc::parser::NodeList
 */

#include "../configuration.hpp"
#include "../error/general_parser_exception.hpp"
#include "../lexer/position.hpp"

#include "node.hpp"
#include "stage_description.hpp"
#include "state.hpp"
#include "wrapper.hpp"

namespace rxc::parser {
	namespace _internal::_parseList {
		// These functions are also being used for parseListStack
		template<class ParserData>
		void parseStart(ParserData&, const Configuration&, State&);

		template<class Item, class Separator>
		void parseItem(std::unique_ptr<Item>&, const Configuration&, State&);

		template<class Item, class Separator>
		void parseSeparator(std::optional<Separator>&, const Configuration&, State&);
	}

	template<class ResultT, class ParserData>
	[[nodiscard]] std::unique_ptr<ResultT> parseList(const Configuration&, State&);

	template<class ResultT, class ParserData>
	[[nodiscard]] ResultT parseListStack(const Configuration&, State&);

	/*!
	 * @brief List node.
	 *
	 * This is an abstraction for implementing all nodes that are a list separated by a specific node with optional trailing separator.
	 *
	 * Such a list must contain at least one element. If this is incorrect, wrap it in @ref NodeMaybe.
	 *
	 * @tparam ItemT The item node.
	 * @tparam SeparatorT The separator node.
	 */
	template<class ItemT, class SeparatorT>
	class NodeList : public Node {
	private:
		class Item;
		class Separator;

		class Separator final {
		public:
			Wrapper<SeparatorT> separator;
			std::unique_ptr<Item> remainder;

		public:
			[[nodiscard]] const ItemT* getItem(std::size_t number) {
				if(this->remainder) {
					return this->remainder->getItem(number);
				} else {
					return nullptr;
				}
			}

			[[nodiscard]] const Node* traverseNode(std::size_t& number) const {
				if(const Node* result = this->separator.traverseNode(number)) {
					return result;
				}
				if(this->remainder) {
					return this->remainder->traverseNode(number);
				} else {
					return nullptr;
				}
			}
			[[nodiscard]] const stage_description::Error* traverseError(std::size_t& number) const {
				if(const stage_description::Error* result = this->separator.traverseError(number)) {
					return result;
				}
				if(this->remainder) {
					return this->remainder->traverseError(number);
				} else {
					return nullptr;
				}
			}

			[[nodiscard]] lexer::Position end() const {
				if(remainder) {
					return this->remainder->end();
				} else {
					return this->separator.end();
				}
			}
		};

		class Item final {
		public:
			Wrapper<ItemT> item;
			std::optional<Separator> remainder;

		public:
			[[nodiscard]] const ItemT* getItem(std::size_t number) const {
				if(number == 0) {
					return &this->item;
				} else if(this->remainder.has_value()) {
					return this->remainder->getItem(number - 1);
				} else {
					return nullptr;
				}
			}

			[[nodiscard]] const Node* traverseNode(std::size_t& number) const {
				if(const Node* result = this->item.traverseNode(number)) {
					return result;
				}
				if(this->remainder.has_value()) {
					return this->remainder->traverseNode(number);
				} else {
					return nullptr;
				}
			}
			[[nodiscard]] const stage_description::Error* traverseError(std::size_t& number) const {
				if(const stage_description::Error* result = this->item.traverseError(number)) {
					return result;
				}
				if(this->remainder.has_value()) {
					return this->remainder->traverseError(number);
				} else {
					return nullptr;
				}
			}

			[[nodiscard]] lexer::Position end() const {
				if(this->remainder.has_value()) {
					return this->remainder->end();
				} else {
					return this->item.end();
				}
			}
		};

		template<class ParserData>
		friend void _internal::_parseList::parseStart(ParserData&, const Configuration&, State&);
		template<class Item, class Separator>
		friend void _internal::_parseList::parseItem(std::unique_ptr<Item>&, const Configuration&, State&);
		template<class Item, class Separator>
		friend void _internal::_parseList::parseSeparator(std::optional<Separator>&, const Configuration&, State&);

		using SelfT = NodeList<ItemT, SeparatorT>;
	public:
		using ParserData = SelfT;

	private:
		Item content;

	public:
		NodeList() = default;

		[[nodiscard]] static inline std::unique_ptr<SelfT> parse(const Configuration& config, State& state) {
			return parseList<SelfT, ParserData>(config, state);
		}
		[[nodiscard]] static inline SelfT parseStack(const Configuration& config, State& state) {
			return parseListStack<SelfT, ParserData>(config, state);
		}

	public:
		[[nodiscard]] const ItemT* getItem(std::size_t number) const {
			return this->content.getItem(number);
		}

	public:
		[[nodiscard]] virtual const Node* traverseNode(std::size_t& number) const final override {
			if(number == 0) {
				return this;
			}
			--number;
			return this->content.traverseNode(number);
		}
		[[nodiscard]] virtual const stage_description::Error* traverseError(std::size_t& number) const final override {
			return this->content.traverseError(number);
		}

		[[nodiscard]] virtual lexer::Position begin() const final override {
			return this->content.item.begin();
		}
		[[nodiscard]] virtual lexer::Position end() const final override {
			return this->content.end();
		}
	};

	namespace _internal::_parseList {
		template<class ParserData>
		void parseStart(ParserData& list, const Configuration& config, State& state) {
			// ParserData is the NodeList instanciation
			list.content.item.parse(config, state);

			try {
				State position = state;
				_internal::_parseList::parseSeparator<typename ParserData::Item, typename ParserData::Separator>(
					list.content.remainder,
					config,
					position
				);
				state = position;
			} catch (const error::GeneralParserException&) {}
		}

		template<class Item, class Separator>
		void parseItem(std::unique_ptr<Item>& item, const Configuration& config, State& state) {
			std::unique_ptr<Item> result = std::make_unique<Item>();
			result->item.parse(config, state);
			item = std::move(result);

			try {
				State position = state;
				parseSeparator<Item, Separator>(item->remainder, config, position);
				state = position;
			} catch(const error::GeneralParserException&) {}
		}

		template<class Item, class Separator>
		void parseSeparator(std::optional<Separator>& separator, const Configuration& config, State& state) {
			auto result = Separator();
			result.separator.parse(config, state);
			separator = std::move(result);

			try {
				State position = state;
				parseItem<Item, Separator>(separator->remainder, config, position);
				state = position;
			} catch(const error::GeneralParserException&) {}
		}
	}

	/*!
	 * @brief Parse list nodes.
	 *
	 * @tparam ResultT The actual node to parse.
	 * @tparam ParserData The parser data as exposed by the list node.
	 * @param config The configuration.
	 * @param state The parser state.
	 */
	template<class ResultT, class ParserData>
	[[nodiscard]] std::unique_ptr<ResultT> parseList(const Configuration& config, State& state) {
		std::unique_ptr<ResultT> result = std::make_unique<ResultT>();
		_internal::_parseList::parseStart<ParserData>(*result, config, state);
		return result;
	}

	/*!
	 * @brief Parse list nodes.
	 *
	 * This is the stack parser version.
	 *
	 * @tparam ResultT The actual node to parse.
	 * @tparam ParserData The parser data as exposed by the list node.
	 * @param config The configuration.
	 * @param state The parser state.
	 */
	template<class ResultT, class ParserData>
	[[nodiscard]] ResultT parseListStack(const Configuration& config, State& state) {
		auto result = ResultT();
		_internal::_parseList::parseStart<ParserData>(result, config, state);
		return result;
	}
}

#endif
