/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_PARSER_POSTFIX_HPP
#define RXC_PARSER_PARSER_POSTFIX_HPP

#include <functional>
#include <memory>

/*!
 * @file
 * @brief Postfix parser.
 * @sa rxc::parser::ParserPostfix
 */

#include "state.hpp"

namespace rxc {
	struct Configuration;
}

namespace rxc::parser {
	/*!
	 * @brief Postfix parser.
	 *
	 * Such functions are like regular parsers, but they take a result
	 * and modify it to have their postfix operator included 
	 * (instead of returning the parsed result).
	 *
	 * This is used for handling the flaw of recursive descant parsers,
	 * unable to handle left recursion.
	 *
	 * Example: Function calls
	 * 	First argument: QualifiedIdentifier(a.b.c)
	 * 	Next tokens: ( param = val )
	 * 	First argument after call: FunctionCall(QualifiedIdentifier(a.b.c), FunctionCallArguments(param = val))
	 */
	template<class ResultT>
	using ParserPostfix = std::function<
		void(
			std::unique_ptr<ResultT>&,
			const Configuration&,
			State&
		)
	>;
}

#endif
