/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_STAGE_SPAN_HPP
#define RXC_STAGE_SPAN_HPP

#include <string>
#include <utility>

/*!
 * @file
 * @brief Position span.
 * @sa rxc::stage::Span
 */

#include "../localized_string.hpp"

namespace rxc::stage {
	/*!
	 * @brief Position span.
	 *
	 * @tparam _PositionT The position type to use.
	 */
	template<class _PositionT>
	class Span final {
	public:
		using PositionT = _PositionT;

	public:
		PositionT start;
		PositionT end;

	public:
		explicit Span(const PositionT& start, const PositionT& end)
			: start(start)
			, end(end)
		{}
		~Span() = default;
	};
}

#endif
