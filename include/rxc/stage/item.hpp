/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_STAGE_ITEM_HPP
#define RXC_STAGE_ITEM_HPP

#include <cstddef>

/*!
 * @file
 * @brief Stage result item.
 * @sa rxc::stage::Item
 */

#include "../export.hpp"

#include "../error/error_general.hpp"

namespace rxc::stage {
	/*!
	 * @brief Stage result item.
	 */
	class LIBRXC_EXPORT Item {
	public:
		Item();
		virtual ~Item();

	public:
		/*!
		 * @brief Traverse the errors.
		 *
		 * This works like the following:
		 * - If this item has a nth error, this decreases n to 0 and returns a pointer to it.
		 * - If this item has exactly n errors, decreases n to 0 and returns nullptr.
		 * - If this item has less than n errors, decreases n by the amount of errors and returns nullptr.
		 *
		 * @param n The number of the error (starting from 0).
		 */
		[[nodiscard]] virtual const error::ErrorGeneral* traverseError(std::size_t& n) const = 0;
	};
}

#endif
