/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_STAGE_DESCRIPTION_DATA_HPP
#define RXC_STAGE_DESCRIPTION_DATA_HPP

#include <string>
#include <utility>

/*!
 * @file
 * @brief Stage description data.
 * @sa rxc::stage::DescriptionData
 */

#include "../localized_string.hpp"

#include "description.hpp"

namespace rxc::stage {
	/*!
	 * @brief Stage description data.
	 *
	 * This also saves some types.
	 *
	 * @tparam _Result The data type of the result.
	 * @tparam _Position The type of positions of errors happening in this stage.
	 * @tparam _StagePrevious The description data type of the previous stage.
	 */
	template<
		class _Result,
		class _Item,
		class _Position,
		class _StagePrevious
	> class DescriptionData final : public Description {
	public:
		using Result = _Result;
		using Item = _Item;
		using Position = _Position;
		using StagePrevious = _StagePrevious;

	public:
		explicit DescriptionData(
			const LocalizedString& name,
			std::string&& config_name,
			const StagePrevious* stage_previous
		)
			: Description {
				.name = name,
				.config_name = std::move(config_name),
				.stage_previous = stage_previous,
			}
		{}

		~DescriptionData() = default;
	};
}

#endif
