/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_STAGE_RESULT_HPP
#define RXC_STAGE_RESULT_HPP

/*!
 * @file
 * @brief Stage result.
 * @sa rxc::stage::Result
 */

#include "../export.hpp"

#include "../error/error_general.hpp"

namespace rxc::stage {
	/*!
	 * @brief Stage result.
	 */
	class LIBRXC_EXPORT Result {
	public:
		Result();
		virtual ~Result();

	public:
		/*!
		 * @brief Get a single error.
		 *
		 * This returns a pointer to the nth error in the result of a step. This should include the errors of the previous steps.
		 *
		 * @param n The number of the error.
		 */
		[[nodiscard]] virtual const error::ErrorGeneral* getError(std::size_t n) const = 0;
	};
}

#endif
