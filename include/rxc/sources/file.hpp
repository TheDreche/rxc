/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_SOURCES_FILE_HPP
#define RXC_SOURCES_FILE_HPP

/*!
 * @file
 * @brief Source file.
 * @sa rxc::sources::File
 */

#include "../export.hpp"

#include "../stage/result.hpp"

#include "character.hpp"
#include "position.hpp"

namespace rxc {
	struct Configuration;
}

namespace rxc::sources {
	/*!
	 * @brief Source file.
	 *
	 * This usually is a file, but actually is an abstract interface allowing, e.g. preprocessing to be done first.
	 */
	class LIBRXC_EXPORT File : public stage::Result {
	public:
		class Iterator;

	public:
		const Configuration* config;

	public:
		explicit File(const Configuration& config);
		virtual ~File();

	public:
		[[nodiscard]] virtual Position begin_position() const = 0;
		[[nodiscard]] virtual Position end_position() const = 0;
		[[nodiscard]] virtual Character dereference(const Position&) const = 0;
		virtual void increase(Position&) const = 0;

	public:
		[[nodiscard]] Iterator begin() const;
		[[nodiscard]] Iterator end() const;
	};

	class LIBRXC_EXPORT File::Iterator final {
	private:
		const File* file;
		Position at;

	private:
		explicit Iterator(const File& file, Position&& at);
		friend File;

	public:
		Iterator& operator++();
		[[nodiscard]] const Position& operator*() const;
		[[nodiscard]] bool operator!=(const Iterator&) const;
	};
}

#endif
