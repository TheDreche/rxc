/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ERROR_ERROR_GENERAL_HPP
#define RXC_ERROR_ERROR_GENERAL_HPP

#include <cstddef>

/*!
 * @file
 * @brief General compilation error.
 * @sa rxc::error::ErrorGeneral
 */

#include "../export.hpp"

#include "../iter/member_getter.hpp"

#include "error_general_description.hpp"
#include "error_general_hint.hpp"
#include "error_general_suggestion.hpp"
#include "error_type.hpp"

/* Example error output (inspired from rustc):
 *
 * 	Error: Type mismatch
 * 	at file.dcl:51:1
 * 	   51 | a = filename;
 * 	      | ^^^^^^^^^^^^
 * 	      | -> Trying to assign incompatible types:
 * 	      |    posixio.Filename cannot be used as compiler.std.Integer
 * 	  Additional hints
 * 	  at file.dcl
 * 	   48 | filename = DEFAULT_FILENAME;
 * 	      | ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * 	      | -> Declaration of filename as posixio.Filename
 * 	   49 | a: compiler.std.Integer;
 * 	      | ~~~~~~~~~~~~~~~~~~~~~~~~
 * 	      | -> Declaration of a as compiler.std.Integer
 * 	  Additional hints
 * 	  at /usr/share/librxc/libraries/posixio/5.4/posixio/Filename.dcl:50:1
 * 	   47 | (
 * 	   48 |     .name: stdlib.String,
 * 	   49 |     verify_filename
 * 	   50 | )
 * 	      | ~~~~~~~~~~~~~~~~~~~~~~~~~
 * 	      | -> Definition of posixio.Filename
 * 	  Suggestion
 * 	  at file.dcl:51:5
 * 	   51 | a = *posixio.Filename.length* filename;
 */

namespace rxc::error {
	/*!
	 * @brief Compilation error or warning.
	 *
	 * This should probably have been called diagnostic.
	 */
	class LIBRXC_EXPORT ErrorGeneral {
	public:
		ErrorGeneral();
		virtual ~ErrorGeneral();

	public:
		[[nodiscard]] virtual const ErrorType* type() const = 0;
		[[nodiscard]] virtual const ErrorGeneralDescription* description() const = 0;

	private:
		[[nodiscard]] virtual const ErrorGeneralHint* hint(std::size_t) const = 0;
		[[nodiscard]] virtual const ErrorGeneralSuggestion* suggestion(std::size_t) const = 0;

	public:
		[[nodiscard]] iter::MemberGetter<const ErrorGeneralHint, ErrorGeneral> hints() const;
		[[nodiscard]] iter::MemberGetter<const ErrorGeneralSuggestion, ErrorGeneral> suggestions() const;
	};
}

#endif
