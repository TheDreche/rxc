/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ERROR_ERROR_STAGED_HPP
#define RXC_ERROR_ERROR_STAGED_HPP

#include <cstddef>

/*!
 * @file
 * @brief Staged compilation error.
 * @sa rxc::error::ErrorStaged
 */

#include "../export.hpp"

#include "error_general.hpp"
#include "error_general_description.hpp"
#include "error_general_hint.hpp"
#include "error_general_suggestion.hpp"
#include "error_staged_description.hpp"
#include "error_staged_hint.hpp"
#include "error_staged_suggestion.hpp"

namespace rxc::error {
	/*!
	 * @brief Compilation error or warning.
	 *
	 * Such errors are sorted into a specific compilation stage.
	 *
	 * @tparam _StageDescription Type metadata about the stage.
	 * @tparam _stage_description Value metadata about the stage.
	 */
	template<class _StageDescription, const _StageDescription* _stage_description>
	class LIBRXC_EXPORT ErrorStaged : public ErrorGeneral {
	public:
		using StageDescription = _StageDescription;
		static constexpr const StageDescription* stage_description = _stage_description;

		using Description = ErrorStagedDescription<StageDescription, stage_description>;
		using Hint = ErrorStagedHint<StageDescription, stage_description>;
		using Suggestion = ErrorStagedSuggestion<StageDescription, stage_description>;

	public:
		ErrorStaged() = default;
		virtual ~ErrorStaged() = default;

	public:
		[[nodiscard]] virtual const Description* staged_description() const = 0;
		[[nodiscard]] virtual const Hint* staged_hint(std::size_t) const = 0;
		[[nodiscard]] virtual const Suggestion* staged_suggestion(std::size_t) const = 0;

	public:
		[[nodiscard]] virtual const ErrorGeneralDescription* description() const final {
			return this->staged_description();
		}
		[[nodiscard]] virtual const ErrorGeneralHint* hint(std::size_t hint_number) const final {
			return this->staged_hint(hint_number);
		}
		[[nodiscard]] virtual const ErrorGeneralSuggestion* suggestion(std::size_t suggestion_number) const final {
			return this->staged_suggestion(suggestion_number);
		}
	};
}

#endif
