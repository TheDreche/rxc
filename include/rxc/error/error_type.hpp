/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ERROR_ERROR_TYPE_HPP
#define RXC_ERROR_ERROR_TYPE_HPP

#include <string>

/*!
 * @file
 * @brief Error type.
 * @sa rxc::error::ErrorType
 */

#include "../export.hpp"

#include "../localized_string.hpp"
#include "../stage/description.hpp"

#include "error_level.hpp"

namespace rxc::error {
	/*!
	 * @brief Error type.
	 */
	class LIBRXC_EXPORT ErrorType final {
	public:
		const LocalizedString* name;
		std::string configuration_name;

		const ErrorLevel* level;
		const stage::Description* stage;
	};
}

#endif
