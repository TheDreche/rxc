/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ERROR_ERROR_GENERAL_DESCRIPTION_HPP
#define RXC_ERROR_ERROR_GENERAL_DESCRIPTION_HPP

/*!
 * @file
 * @brief General error description.
 * @sa rxc::error::ErrorGeneralDescription
 */

#include "../export.hpp"

#include "../localized_string.hpp"

namespace rxc::error {
	/*!
	 * @brief General error description.
	 *
	 * This is the text below the source code describing the error.
	 */
	class LIBRXC_EXPORT ErrorGeneralDescription {
	public:
		ErrorGeneralDescription();
		virtual ~ErrorGeneralDescription();

	public:
		[[nodiscard]] virtual const LocalizedString& text() const = 0;
	};
}

#endif
