/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ERROR_ERROR_STAGED_HINT_HPP
#define RXC_ERROR_ERROR_STAGED_HINT_HPP

/*!
 * @file
 * @brief Staged error hint.
 * @sa rxc::error::ErrorStagedHint
 */

#include "../export.hpp"

#include "error_general_hint.hpp"

namespace rxc::error {
	/*!
	 * @brief Staged error hint.
	 *
	 * Such error hints are sorted to a specific compilation stage.
	 *
	 * @tparam _StageDescription Type metadata about the stage.
	 * @tparam _stage_description Value metadata about the stage.
	 */
	template<class _StageDescription, const _StageDescription* _stage_description>
	class LIBRXC_EXPORT ErrorStagedHint : public ErrorGeneralHint {
	public:
		using StageDescription = _StageDescription;
		static constexpr const StageDescription* stage_description = _stage_description;
		using Position = typename StageDescription::StagePrevious::Position;
		using File = typename StageDescription::StagePrevious::Result;

	public:
		const File* file;
		Position position;

	public:
		explicit ErrorStagedHint(const File* file, const Position& position)
			: file(file)
			, position(position)
		{}
		virtual ~ErrorStagedHint() = default;
	};
}

#endif
