/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ERROR_INVALID_TOKEN_START_ERROR_HPP
#define RXC_ERROR_INVALID_TOKEN_START_ERROR_HPP

#include <cstddef>

/*!
 * @file
 * @brief Invalid token start error.
 * @sa rxc::error::InvalidTokenStartError
 */

#include "../export.hpp"

#include "../localized_string.hpp"
#include "../tokenizer/stage_description.hpp"

#include "error_type.hpp"

namespace rxc::error {
	/*!
	 * @brief Invalid token start error.
	 */
	class LIBRXC_EXPORT InvalidTokenStartError final : public tokenizer::stage_description::Error {
	private:
		class EDescription final : public Description {
		public:
			explicit EDescription(const File*, const Position&);

		public:
			[[nodiscard]] virtual const LocalizedString& text() const override;
		};

	private:
		static const ErrorType error_type;

	private:
		EDescription error_description;

	public:
		explicit InvalidTokenStartError(const EDescription::File*, const EDescription::Position& position);

	public:
		[[nodiscard]] virtual const ErrorType* type() const override;
		[[nodiscard]] virtual const Description* staged_description() const override;
		[[nodiscard]] virtual const Hint* staged_hint(std::size_t) const override;
		[[nodiscard]] virtual const Suggestion* staged_suggestion(std::size_t) const override;
	};
}

#endif
