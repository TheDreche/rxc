/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ERROR_ERROR_LEVEL_HPP
#define RXC_ERROR_ERROR_LEVEL_HPP

#include <string>

/*!
 * @file
 * @brief Error level.
 * @sa rxc::error::ErrorLevel
 */

#include "../export.hpp"

#include "../localized_string.hpp"

namespace rxc::error {
	/*!
	 * @brief Error level.
	 */
	class LIBRXC_EXPORT ErrorLevel final {
	public:
		const ErrorLevel* most_similar;
		bool higher_than_most_similar;

		std::string configuration_name;
		const LocalizedString* name;
	};
}

#endif
