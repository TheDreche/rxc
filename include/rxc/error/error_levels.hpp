/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_ERROR_ERROR_LEVELS_HPP
#define RXC_ERROR_ERROR_LEVELS_HPP

/*!
 * @file
 * @brief Error levels.
 * @sa rxc::error::error_levels
 */

#include "../export.hpp"

#include "error_level.hpp"

namespace rxc::error {
	/*!
	 * @brief Error levels.
	 */
	namespace error_levels {
		extern LIBRXC_EXPORT const ErrorLevel error;
		extern LIBRXC_EXPORT const ErrorLevel warning;
	}
}

#endif
