/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CONFIGURATION_HPP
#define RXC_CONFIGURATION_HPP

#include <vector>

/*!
 * @file
 * @brief librxc configuration.
 * @sa rxc::Configuration
 */

#include "export.hpp"

#include "tokenizer/token_lexer.hpp"
#include "lexer/token_transformer.hpp"

#include "parser/parser_heap.hpp"
#include "parser/parser_postfix.hpp"

namespace rxc {
	namespace cst {
		class BlockStatement;
		class Expression;
		class NamespaceReference;
		class TypeDefinitionStatement;
		class TypeInstanciationStatement;
	}
}

namespace rxc {
	/*!
	 * @brief librxc configuration.
	 *
	 * These are some general purpose configuration settings.
	 *
	 * The default configuration can be accessed by the return value of the
	 * defaultConfiguration() static member function.
	 *
	 * Please note: When not using the default configuration as a base, you
	 * may not be able to parse correctly (because the list of alternatives
	 * for parsing expressions or other things is empty). This may lead to
	 * UB!
	 */
	struct LIBRXC_EXPORT Configuration final {
		[[nodiscard]] static Configuration defaultConfiguration();

		// Lexer:
		std::vector<tokenizer::TokenLexer> token_lexers;

		// Token discarder:
		std::vector<lexer::TokenTransformer> token_transformers;

		// Expressions:
		std::vector<parser::ParserHeap<cst::Expression>> expression_parsers;
		std::vector<parser::ParserPostfix<cst::Expression>> expression_postfix_operator_parsers;

		std::vector<parser::ParserHeap<cst::Expression>> block_content_expression_parsers;
		std::vector<parser::ParserPostfix<cst::Expression>> block_content_expression_postfix_parsers;

		// Namespace references:
		std::vector<parser::ParserHeap<cst::NamespaceReference>> namespace_reference_parsers;

		// Statements:
		std::vector<parser::ParserHeap<cst::BlockStatement>> block_statement_parsers;
		std::vector<parser::ParserPostfix<cst::BlockStatement>> block_statement_postfix_parsers;

		// Type definition statements:
		std::vector<parser::ParserHeap<cst::TypeDefinitionStatement>> type_definition_statement_parsers;
		std::vector<parser::ParserPostfix<cst::TypeDefinitionStatement>> type_definition_statement_postfix_parsers;

		// Type instanciation statements:
		std::vector<parser::ParserHeap<cst::TypeInstanciationStatement>> type_instanciation_statement_parsers;
		std::vector<parser::ParserPostfix<cst::TypeInstanciationStatement>> type_instanciation_statement_postfix_parsers;
	};
}

#endif
