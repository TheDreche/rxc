/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_LEXER_LITERAL_CURLY_BRACKET_LEFT_HPP
#define RXC_LEXER_LITERAL_CURLY_BRACKET_LEFT_HPP

#include <memory>
#include <vector>

/*!
 * @file
 * @brief CurlyBracketLeft token.
 * @sa rxc::lexer::LiteralCurlyBracketLeft
 */

#include "../export.hpp"

#include "../tokenizer/file.hpp"
#include "../tokenizer/position.hpp"

#include "token.hpp"

namespace rxc::lexer {
	/*!
	 * @brief CurlyBracketLeft token.
	 */
	class LIBRXC_EXPORT LiteralCurlyBracketLeft final : public Token {
	public:
		explicit LiteralCurlyBracketLeft(const tokenizer::Position&);

	public:
		static void transform(
			const tokenizer::File&,
			tokenizer::Position&,
			std::vector<std::unique_ptr<const Token>>&
		);
	};
}

#endif
