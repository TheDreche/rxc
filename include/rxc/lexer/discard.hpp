/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_LEXER_DISCARD_HPP
#define RXC_LEXER_DISCARD_HPP

#include <memory>
#include <vector>

/*!
 * @file
 * @brief Discard specific tokens.
 * @sa rxc::lexer::discard
 */

#include "../tokenizer/file.hpp"
#include "../tokenizer/position.hpp"
#include "../tokenizer/token.hpp"

#include "token.hpp"

namespace rxc::lexer {
	template<class TokenT>
	void discard(
		const tokenizer::File& file,
		tokenizer::Position& position,
		std::vector<std::unique_ptr<const Token>>&
	) {
		if(file.dereference(position)->isType<TokenT>()) {
			file.increase(position);
		}
	}
}

#endif
