/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_LEXER_FILE_HPP
#define RXC_LEXER_FILE_HPP

#include <cstddef>
#include <memory>
#include <vector>

/*!
 * @file
 * @brief Lexed file.
 * @sa rxc::lexer::File
 */

#include "../export.hpp"

#include "../tokenizer/file.hpp"
#include "../stage/result.hpp"

#include "position.hpp"
#include "stage_description.hpp"

namespace rxc::lexer {
	class Token;

	/*!
	 * @brief Lexed file.
	 */
	class LIBRXC_EXPORT File final : public stage::Result {
	public:
		class Iterator;
	private:
		using TokenWrapper = std::unique_ptr<const Token>;
		using TokenVector = std::vector<TokenWrapper>;

	public:
		std::unique_ptr<const tokenizer::File> tokenized;
	private:
		mutable TokenVector tokens;
		friend Position;

	public:
		/*!
		 * @brief Lex a file.
		 *
		 * The configuration (saved in file.read_file.config) has to ensure that there is a pattern for all possible types. If not this may lead to undefined behaviour.
		 *
		 * @param file The tokenized file.
		 */
		explicit File(std::unique_ptr<const tokenizer::File>&& file);

	public:
		[[nodiscard]] virtual const stage_description::Error* getError(std::size_t) const override;

		[[nodiscard]] Iterator begin() const;
		[[nodiscard]] Iterator end() const;

		void increase(Position&) const;
		[[nodiscard]] const Token* dereference(const Position&) const;

	private:
		void lex_all() const;
	};

	class LIBRXC_EXPORT File::Iterator {
	private:
		Position position;

	private:
		explicit Iterator(Position&& pos);
		friend File;

	public:
		[[nodiscard]] Position operator*() const;
		Iterator& operator++();
		[[nodiscard]] bool operator==(const Iterator&) const;
		[[nodiscard]] bool operator!=(const Iterator&) const;
	};
}

#endif
