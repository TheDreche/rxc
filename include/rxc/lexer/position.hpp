/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_LEXER_POSITION_HPP
#define RXC_LEXER_POSITION_HPP

#include <cstddef>

/*!
 * @file
 * @brief Lexer position.
 * @sa rxc::lexer::Position
 */

#include "../export.hpp"

#include "../stage/position.hpp"

namespace rxc::lexer {
	class File;
}

namespace rxc::lexer {
	/*!
	 * @brief Lexer position.
	 */
	class LIBRXC_EXPORT Position final : public stage::Position {
	private:
		std::size_t position;

	private:
		explicit Position(std::size_t);
		friend File;

	public:
		Position();
		[[nodiscard]] bool operator==(const Position&) const;
		[[nodiscard]] bool operator!=(const Position&) const;
		[[nodiscard]] bool operator<=(const Position&) const;
		[[nodiscard]] bool operator>=(const Position&) const;
		[[nodiscard]] bool operator< (const Position&) const;
		[[nodiscard]] bool operator> (const Position&) const;
	};
}

#endif
