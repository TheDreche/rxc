/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_LEXER_TOKEN_HPP
#define RXC_LEXER_TOKEN_HPP

#include <cassert>
#include <cstddef>

/*!
 * @file
 * @brief Token.
 * @sa rxc::lexer::Token
 */

#include "../export.hpp"

#include "../stage/item.hpp"
#include "../tokenizer/position.hpp"

#include "stage_description.hpp"

namespace rxc::lexer {
	/*!
	 * @brief Token.
	 */
	class LIBRXC_EXPORT Token : public stage::Item {
	public:
		tokenizer::Position position;

	protected:
		explicit Token(const tokenizer::Position&);

	public:
		virtual ~Token();

	public:
		[[nodiscard]] const stage_description::Error* traverseError(std::size_t&) const final override;

	public:
		template<class TokenT>
		[[nodiscard]] bool isType() const {
			return dynamic_cast<const TokenT*>(this) != nullptr;
		}
		template<class TokenT>
		[[nodiscard]] const TokenT* asType() const {
			const TokenT* r = dynamic_cast<const TokenT*>(this);
			assert(r != nullptr);
			return r;
		}
	};
}

#endif
