/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_LEXER_TOKEN_TRANSFORMER_HPP
#define RXC_LEXER_TOKEN_TRANSFORMER_HPP

#include <functional>
#include <memory>
#include <vector>

/*!
 * @file
 * @brief Token transformer.
 * @sa rxc::tokenizer::TokenTransformer
 */

#include "../tokenizer/file.hpp"
#include "../tokenizer/position.hpp"

#include "token.hpp"

namespace rxc::lexer {
	/*!
	 * @brief Token transformer.
	 *
	 * This is a function taking a file, a position and the list of result tokens.
	 *
	 * This checks whether the following tokens (from the given position in the file) match a specific pattern. If they do, it increases the position and transforms the tokens and adds the relevant part to the list. If it doesn't match, such functiona are a no-op.
	 */
	using TokenTransformer = std::function<
		void(
			const tokenizer::File&,
			tokenizer::Position&,
			std::vector<std::unique_ptr<const Token>>&
		)
	>;
}

#endif
