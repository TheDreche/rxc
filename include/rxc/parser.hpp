/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_PARSER_HPP
#define RXC_PARSER_HPP

/*!
 * @file
 * @brief Parser utilities.
 * @sa rxc::parser
 */

#include "parser/file.hpp"
#include "parser/node.hpp"
#include "parser/node_compound.hpp"
#include "parser/node_compound_crtp.hpp"
#include "parser/node_decision_crtp.hpp"
#include "parser/node_list.hpp"
#include "parser/node_list_crtp.hpp"
#include "parser/node_maybe.hpp"
#include "parser/node_maybe_crtp.hpp"
#include "parser/node_token.hpp"
#include "parser/node_token_crtp.hpp"
#include "parser/node_wrapper.hpp"
#include "parser/parse_compound.hpp"
#include "parser/parse_compound_stack.hpp"
#include "parser/parse_compound_postfix.hpp"
#include "parser/parse_decision.hpp"
#include "parser/parse_list.hpp"
#include "parser/parse_list_stack.hpp"
#include "parser/parse_maybe.hpp"
#include "parser/parse_maybe_stack.hpp"
#include "parser/parse_postfix.hpp"
#include "parser/parse_token.hpp"
#include "parser/parse_token_stack.hpp"
#include "parser/parse_wrapper.hpp"
#include "parser/parse_wrapper_stack.hpp"
#include "parser/parser_heap.hpp"
#include "parser/parser_postfix.hpp"
#include "parser/parser_stack.hpp"
#include "parser/position.hpp"
#include "parser/stage_description.hpp"
#include "parser/state.hpp"
#include "parser/wrapper.hpp"

namespace rxc {
	/*!
	 * @brief Collection of parser utilities.
	 *
	 * The contents are like the stage management for parsing; the actual result is the concrete syntax tree (not saved in here; see @ref rxc::cst for that).
	 *
	 * About the content and names:
	 * - Wrapper<NodeT>: A wrapper saving a CST node of the given type
	 * - Names starting with `Node` are abstractions over common kinds of nodes (abstracting away parsing and related things):
	 *   + `NodeCompound`: Nodes that compose out of other nodes
	 *   + `NodeMaybe`: Nodes that maybe contain another node or are empty
	 *   + `NodeList`: Nodes that are a list of nodes, separated by other nodes with optional terminating separator
	 * - Names starting with `parser` are the type of functions that are able to parse a specific node kind:
	 *   + `ParserStack`: Parse and return result directly
	 *   + `ParserHeap`: Parse and return result wrapped in std::unique_ptr
	 *   + `ParserPostfix`: Take first item, parse remainder, wrap everything in std::unique_ptr.
	 * - Names starting with `parse` are template functions for parsing a given kind of node:
	 *   + When ending with `Stack`, they parse and return the result directly
	 *   + When ending with `Postfix`, they take the first item (as std::unique_ptr) and return a new std::unique_ptr of the same type containing more tokens
	 *   + Otherwise, will just parse and return a std::unique_ptr to the parsed node
	 *   + In between `parser` and `Stack`/`Postfix`, there is the name of the kind of node being parsed
	 *
	 * Then, there also is the type of node which may be one of multiple compound node: An expression could be a function call, a simple variable or one of many other things. They are represented only within the CST namespace, but have a parsing function specified in here as `parseDecision`.
	 */
	namespace parser {}
}

#endif
