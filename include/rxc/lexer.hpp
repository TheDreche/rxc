/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_LEXER_HPP
#define RXC_LEXER_HPP

/*!
 * @file
 * @brief Token remover.
 * @sa rxc::lexer
 */

#include "lexer/discard.hpp"
#include "lexer/file.hpp"
#include "lexer/invalid.hpp"
#include "lexer/literal_colon.hpp"
#include "lexer/literal_comma.hpp"
#include "lexer/literal_curly_bracket_left.hpp"
#include "lexer/literal_curly_bracket_right.hpp"
#include "lexer/literal_dot.hpp"
#include "lexer/literal_eof.hpp"
#include "lexer/literal_equals.hpp"
#include "lexer/literal_identifier.hpp"
#include "lexer/literal_parentheses_left.hpp"
#include "lexer/literal_parentheses_right.hpp"
#include "lexer/literal_semicolon.hpp"
#include "lexer/position.hpp"
#include "lexer/span.hpp"
#include "lexer/stage_description.hpp"
#include "lexer/token.hpp"
#include "lexer/token_transformer.hpp"

namespace rxc {
	/*!
	 * @brief Token remover.
	 *
	 * This discards tokens to be ignored, such as whitespace and comments.
	 */
	namespace lexer {}
}

#endif
