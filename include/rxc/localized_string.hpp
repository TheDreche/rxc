/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_LOCALIZED_STRING_HPP
#define RXC_LOCALIZED_STRING_HPP

#include <string>

/*!
 * @file
 * @brief Localized string.
 * @sa rxc::LocalizedString
 */

#include "export.hpp"

namespace rxc {
	/*!
	 * @brief Localized string.
	 */
	class LIBRXC_EXPORT LocalizedString {
	public:
		LocalizedString(std::string&& original);
		virtual ~LocalizedString();

		std::string posix_locale;
		[[nodiscard]] virtual std::string localized() const = 0;
	};
}

#endif
