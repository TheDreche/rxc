/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_BLOCK_CONTENT_HPP
#define RXC_CST_BLOCK_CONTENT_HPP

/*!
 * @file
 * @brief CST block content node.
 * @sa rxc::cst::BlockContent
 */

#include "../export.hpp"

#include "../parser/node_compound_crtp.hpp"
#include "../parser/node_maybe.hpp"
#include "../parser/node_wrapper.hpp"
#include "../parser/parser_heap.hpp"

#include "block_statement_list.hpp"
#include "expression.hpp"

namespace rxc::cst {
	namespace _internal::_BlockContent {
		extern const parser::ParserHeap<Expression> parseExpression;
	}

	/*!
	 * @brief CST block content node.
	 */
	class LIBRXC_EXPORT BlockContent final : public parser::NodeCompoundCrtp<
		BlockContent,
		parser::NodeMaybe<BlockStatementList>,
		parser::NodeWrapper<
			Expression,
			&_internal::_BlockContent::parseExpression
		>
	> {
	public:
		BlockContent();

		using CRTP::parse;
		using CRTP::parsePostfix;
		using CRTP::parseStack;
	};
}

#endif
