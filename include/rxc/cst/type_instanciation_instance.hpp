/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_TYPE_INSTANCIATION_INSTANCE_HPP
#define RXC_CST_TYPE_INSTANCIATION_INSTANCE_HPP

/*!
 * @file
 * @brief CST type instanciation instance node.
 * @sa rxc::cst::TypeInstanciationInstance
 */

#include "../export.hpp"

#include "../parser/node_compound_crtp.hpp"

#include "expression.hpp"
#include "type_instanciation_statement.hpp"

namespace rxc::cst {
	/*!
	 * @brief CST type instanciation instance node.
	 *
	 * Using this construct, one can create a type having all properties of the type specified by an expression.
	 */
	class LIBRXC_EXPORT TypeInstanciationInstance final : public TypeInstanciationStatement, public parser::NodeCompoundCrtp<
		TypeInstanciationInstance,
		Expression
	> {
	public:
		TypeInstanciationInstance();

		using CRTP::parse;
		using CRTP::parsePostfix;
		using CRTP::parseStack;
	};
}

#endif
