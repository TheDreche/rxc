/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_TYPE_DEFINITION_STATEMENT_HPP
#define RXC_CST_TYPE_DEFINITION_STATEMENT_HPP

/*!
 * @file
 * @brief CST type definition statement node.
 * @sa rxc::cst::TypeDefinitionStatement
 */

#include "../export.hpp"

#include "../configuration.hpp"
#include "../parser/node_decision_crtp.hpp"

namespace rxc::cst {
	class TypeDefinitionStatement;
	namespace _internal::_TypeDefinitionStatement {
		using CRTP = parser::NodeDecisionCrtp<
			TypeDefinitionStatement,
			&Configuration::type_definition_statement_parsers,
			&Configuration::type_definition_statement_postfix_parsers
		>;
	}

	/*!
	 * @brief CST type definition statement node.
	 *
	 * A type definition statement specifies properties of a type.
	 */
	class LIBRXC_EXPORT TypeDefinitionStatement : public _internal::_TypeDefinitionStatement::CRTP {
	public:
		TypeDefinitionStatement();

		using _internal::_TypeDefinitionStatement::CRTP::parse;
	};
}

#endif
