/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_FUNCTION_DEFINITION_HPP
#define RXC_CST_FUNCTION_DEFINITION_HPP

/*!
 * @file
 * @brief CST function definition node.
 * @sa rxc::cst::FunctionDefinition
 */

#include "../export.hpp"

#include "../parser/node_compound_crtp.hpp"
#include "../parser/node_maybe.hpp"

#include "block_content.hpp"
#include "expression.hpp"
#include "function_definition_annotation_return_type.hpp"
#include "function_definition_parameter_list.hpp"
#include "literal_curly_bracket_left.hpp"
#include "literal_curly_bracket_right.hpp"
#include "literal_parentheses_left.hpp"
#include "literal_parentheses_right.hpp"

namespace rxc::cst {
	/*!
	 * @brief CST function definition node.
	 */
	class LIBRXC_EXPORT FunctionDefinition final : public Expression, public parser::NodeCompoundCrtp<
		FunctionDefinition,
		LiteralParenthesesLeft,
		FunctionDefinitionParameterList,
		LiteralParenthesesRight,
		parser::NodeMaybe<FunctionDefinitionAnnotationReturnType>,
		LiteralCurlyBracketLeft,
		BlockContent,
		LiteralCurlyBracketRight
	> {
	public:
		FunctionDefinition();

		using CRTP::parse;
		using CRTP::parsePostfix;
		using CRTP::parseStack;
	};
}

#endif
