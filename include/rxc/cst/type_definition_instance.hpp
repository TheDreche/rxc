/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_TYPE_DEFINITION_INSTANCE_HPP
#define RXC_CST_TYPE_DEFINITION_INSTANCE_HPP

/*!
 * @file
 * @brief CST type definition instance node.
 * @sa rxc::cst::TypeDefinitionInstance
 */

#include "../export.hpp"

#include "../parser/node_compound_crtp.hpp"

#include "expression.hpp"
#include "type_definition_statement.hpp"

namespace rxc::cst {
	/*!
	 * @brief CST type definition instance node.
	 */
	class LIBRXC_EXPORT TypeDefinitionInstance final : public TypeDefinitionStatement, public parser::NodeCompoundCrtp<
		TypeDefinitionInstance,
		Expression
	> {
	public:
		TypeDefinitionInstance();

		using CRTP::parse;
		using CRTP::parsePostfix;
		using CRTP::parseStack;
	};
}

#endif
