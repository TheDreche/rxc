/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_FUNCTION_DEFINITION_PARAMETER_LIST_HPP
#define RXC_CST_FUNCTION_DEFINITION_PARAMETER_LIST_HPP

/*!
 * @file
 * @brief CST function definition parameter list node.
 * @sa rxc::cst::FunctionDefinitionParameterList
 */

#include "../export.hpp"

#include "../parser/node_list_crtp.hpp"

#include "function_definition_parameter.hpp"
#include "literal_comma.hpp"

namespace rxc::cst {
	/*!
	 * @brief CST function definition parameter list node.
	 */
	class LIBRXC_EXPORT FunctionDefinitionParameterList final : public parser::NodeListCrtp<
		FunctionDefinitionParameterList,
		FunctionDefinitionParameter,
		LiteralComma
	> {
	public:
		FunctionDefinitionParameterList();

		using CRTP::parse;
		using CRTP::parseStack;
	};
}

#endif
