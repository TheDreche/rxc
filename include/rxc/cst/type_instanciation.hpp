/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_TYPE_INSTANCIATION_HPP
#define RXC_CST_TYPE_INSTANCIATION_HPP

/*!
 * @file
 * @brief CST type instanciation node.
 * @sa rxc::cst::TypeInstanciation
 */

#include "../export.hpp"

#include "../parser/node_compound_crtp.hpp"

#include "expression.hpp"
#include "literal_parentheses_left.hpp"
#include "literal_parentheses_right.hpp"
#include "type_instanciation_statement_list.hpp"

namespace rxc::cst {
	/*!
	 * @brief CST type instanciation node.
	 */
	class LIBRXC_EXPORT TypeInstanciation final : public Expression, public parser::NodeCompoundCrtp<
		TypeInstanciation,
		Expression,
		LiteralParenthesesLeft,
		TypeInstanciationStatementList,
		LiteralParenthesesRight
	> {
	public:
		TypeInstanciation();

		using CRTP::parse;
		using CRTP::parsePostfix;
		using CRTP::parseStack;
	};
}

#endif
