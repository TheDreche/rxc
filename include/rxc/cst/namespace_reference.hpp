/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_NAMESPACE_REFERENCE_HPP
#define RXC_CST_NAMESPACE_REFERENCE_HPP

/*!
 * @file
 * @brief CST namespace reference node.
 * @sa rxc::cst::NamespaceReference
 */

#include "../export.hpp"

#include "../configuration.hpp"
#include "../parser/node_decision_crtp.hpp"

namespace rxc::cst {
	class NamespaceReference;
	namespace _internal::_NamespaceReference {
		using CRTP = parser::NodeDecisionCrtp<
			NamespaceReference,
			&Configuration::namespace_reference_parsers
		>;
	}

	/*!
	 * @brief CST namespace reference node.
	 */
	class LIBRXC_EXPORT NamespaceReference : private _internal::_NamespaceReference::CRTP {
	public:
		NamespaceReference();

		using _internal::_NamespaceReference::CRTP::parse;
	};
}

#endif
