/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_NAMESPACE_REFERENCE_RELATIVE_HPP
#define RXC_CST_NAMESPACE_REFERENCE_RELATIVE_HPP

/*!
 * @file
 * @brief CST relative namespace reference node.
 * @sa rxc::cst::NamespaceReferenceRelative
 */

#include "../export.hpp"

#include "../parser/node_compound_crtp.hpp"

#include "literal_dot.hpp"
#include "literal_identifier.hpp"
#include "namespace_reference.hpp"
#include "rxc/parser/node_maybe.hpp"

namespace rxc::cst {
	class NamespaceReferenceRelative;

	/*!
	 * @brief CST relative namespace reference subnamespace node.
	 */
	class LIBRXC_EXPORT NamespaceReferenceRelativeSubnamespace final : public parser::NodeCompoundCrtp<
		NamespaceReferenceRelativeSubnamespace,
		LiteralIdentifier,
		NamespaceReferenceRelative
	> {
	public:
		NamespaceReferenceRelativeSubnamespace();

		using CRTP::parse;
		using CRTP::parsePostfix;
		using CRTP::parseStack;
	};

	/*!
	 * @brief CST relative namespace reference node.
	 *
	 * This means the references of namespaces found in files. A namespace reference always ends in a dot.
	 */
	class LIBRXC_EXPORT NamespaceReferenceRelative final : public NamespaceReference, public parser::NodeCompoundCrtp<
		NamespaceReferenceRelative,
		LiteralDot,
		parser::NodeMaybe<NamespaceReferenceRelativeSubnamespace>
	> {
	public:
		NamespaceReferenceRelative();

		using CRTP::parse;
		using CRTP::parsePostfix;
		using CRTP::parseStack;
	};
}

#endif
