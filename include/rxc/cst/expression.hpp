/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_EXPRESSION_HPP
#define RXC_CST_EXPRESSION_HPP

/*!
 * @file
 * @brief CST expression node.
 * @sa rxc::cst::Expression
 */

#include "../export.hpp"

#include "../configuration.hpp"
#include "../parser/node_decision_crtp.hpp"

namespace rxc::cst {
	class Expression;
	namespace _internal::_Expression {
		using CRTP = parser::NodeDecisionCrtp<
			Expression,
			&rxc::Configuration::expression_parsers,
			&rxc::Configuration::expression_postfix_operator_parsers
		>;
	}

	/*!
	 * @brief CST expression node.
	 */
	class LIBRXC_EXPORT Expression : private _internal::_Expression::CRTP {
	public:
		Expression();

		using _internal::_Expression::CRTP::parse;
	};
}

#endif
