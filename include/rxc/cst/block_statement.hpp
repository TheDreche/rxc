/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_BLOCK_STATEMENT_HPP
#define RXC_CST_BLOCK_STATEMENT_HPP

/*!
 * @file
 * @brief CST block statement node.
 * @sa rxc::cst::BlockStatement
 */

#include "../export.hpp"

#include "../configuration.hpp"
#include "../parser/node_decision_crtp.hpp"

namespace rxc::cst {
	class BlockStatement;
	namespace _internal::_BlockStatement {
		using CRTP = parser::NodeDecisionCrtp<
			BlockStatement,
			&Configuration::block_statement_parsers,
			&Configuration::block_statement_postfix_parsers
		>;
	}
	/*!
	 * @brief CST block statement node.
	 */
	class LIBRXC_EXPORT BlockStatement : private _internal::_BlockStatement::CRTP {
	public:
		BlockStatement();

		using _internal::_BlockStatement::CRTP::parse;
	};
}

#endif
