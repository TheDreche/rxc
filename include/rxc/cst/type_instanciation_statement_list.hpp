/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_TYPE_INSTANCIATION_STATEMENT_LIST_HPP
#define RXC_CST_TYPE_INSTANCIATION_STATEMENT_LIST_HPP

/*!
 * @file
 * @brief CST type instanciation statement list node.
 * @sa rxc::cst::TypeInstanciationStatementList
 */

#include "../export.hpp"

#include "../parser/node_list.hpp"
#include "../parser/node_maybe_crtp.hpp"

#include "literal_comma.hpp"
#include "type_instanciation_statement.hpp"

namespace rxc::cst {
	/*!
	 * @brief CST type instanciation statement list node.
	 */
	class LIBRXC_EXPORT TypeInstanciationStatementList final : public parser::NodeMaybeCrtp<
		TypeInstanciationStatementList,
		parser::NodeList<
			TypeInstanciationStatement,
			LiteralComma
		>
	> {
	public:
		TypeInstanciationStatementList();

		using CRTP::parse;
		using CRTP::parseStack;
	};
}

#endif
