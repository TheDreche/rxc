/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_LITERAL_CURLY_BRACKET_RIGHT_HPP
#define RXC_CST_LITERAL_CURLY_BRACKET_RIGHT_HPP

/*!
 * @file
 * @brief CST CurlyBracketRight token node.
 * @sa rxc::cst::LiteralCurlyBracketRight
 */

#include "../lexer/literal_curly_bracket_right.hpp"
#include "../parser/node_token_crtp.hpp"

namespace rxc::cst {
	class LiteralCurlyBracketRight final : public parser::NodeTokenCrtp<LiteralCurlyBracketRight, lexer::LiteralCurlyBracketRight> {};
}

#endif
