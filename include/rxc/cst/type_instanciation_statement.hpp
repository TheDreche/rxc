/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_TYPE_INSTANCIATION_STATEMENT_HPP
#define RXC_CST_TYPE_INSTANCIATION_STATEMENT_HPP

/*!
 * @file
 * @brief CST type instanciation statement node.
 * @sa rxc::cst::TypeInstanciationStatement
 */

#include "../export.hpp"

#include "../configuration.hpp"
#include "../parser/node_decision_crtp.hpp"

namespace rxc::cst {
	class TypeInstanciationStatement;
	namespace _internal::_TypeInstanciationStatement {
		using CRTP = parser::NodeDecisionCrtp<
			TypeInstanciationStatement,
			&Configuration::type_instanciation_statement_parsers,
			&Configuration::type_instanciation_statement_postfix_parsers
		>;
	}

	/*!
	 * @brief CST type instanciation statement node.
	 *
	 * A type instanciation statement specifies properties of a type.
	 */
	class LIBRXC_EXPORT TypeInstanciationStatement : public _internal::_TypeInstanciationStatement::CRTP {
	public:
		TypeInstanciationStatement();

		using _internal::_TypeInstanciationStatement::CRTP::parse;
	};
}

#endif
