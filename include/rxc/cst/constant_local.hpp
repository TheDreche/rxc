/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_CONSTANT_LOCAL_HPP
#define RXC_CST_CONSTANT_LOCAL_HPP

/*!
 * @file
 * @brief CST local constant.
 * @sa rxc::cst::ConstantLocal
 */

#include "../export.hpp"

#include "../parser/node_compound_crtp.hpp"

#include "expression.hpp"
#include "literal_identifier.hpp"

namespace rxc::cst {
	/*!
	 * @brief CST local constant.
	 */
	class LIBRXC_EXPORT ConstantLocal final : public Expression, public parser::NodeCompoundCrtp<
		ConstantLocal,
		LiteralIdentifier
	> {
	public:
		ConstantLocal();

		using CRTP::parse;
		using CRTP::parsePostfix;
		using CRTP::parseStack;
	};
}

#endif
