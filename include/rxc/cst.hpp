/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_CST_HPP
#define RXC_CST_HPP

/*!
 * @file
 * @brief CST nodes.
 * @sa rxc::cst
 */

#include "cst/block.hpp"
#include "cst/block_comma.hpp"
#include "cst/block_content.hpp"
#include "cst/block_statement.hpp"
#include "cst/block_statement_list.hpp"
#include "cst/block_statement_terminated.hpp"
#include "cst/constant_declaration.hpp"
#include "cst/constant_definition.hpp"
#include "cst/constant_local.hpp"
#include "cst/constant_qualified.hpp"
#include "cst/expression.hpp"
#include "cst/function_call.hpp"
#include "cst/function_call_parameter.hpp"
#include "cst/function_call_parameter_list.hpp"
#include "cst/function_call_unnamed.hpp"
#include "cst/function_definition.hpp"
#include "cst/function_definition_annotation_return_type.hpp"
#include "cst/function_definition_parameter.hpp"
#include "cst/function_definition_parameter_assignment.hpp"
#include "cst/function_definition_parameter_list.hpp"
#include "cst/function_type.hpp"
#include "cst/function_type_parameter.hpp"
#include "cst/function_type_parameter_list.hpp"
#include "cst/literal_colon.hpp"
#include "cst/literal_comma.hpp"
#include "cst/literal_curly_bracket_left.hpp"
#include "cst/literal_curly_bracket_right.hpp"
#include "cst/literal_dot.hpp"
#include "cst/literal_equals.hpp"
#include "cst/literal_identifier.hpp"
#include "cst/literal_parentheses_left.hpp"
#include "cst/literal_parentheses_right.hpp"
#include "cst/literal_semicolon.hpp"
#include "cst/namespace_reference.hpp"
#include "cst/namespace_reference_absolute.hpp"
#include "cst/namespace_reference_relative.hpp"
#include "cst/namespace_reference_relative_subnamespace.hpp"
#include "cst/type_definition.hpp"
#include "cst/type_definition_instance.hpp"
#include "cst/type_definition_property.hpp"
#include "cst/type_definition_statement.hpp"
#include "cst/type_definition_statement_list.hpp"
#include "cst/type_instanciation.hpp"
#include "cst/type_instanciation_instance.hpp"
#include "cst/type_instanciation_property.hpp"
#include "cst/type_instanciation_statement.hpp"
#include "cst/type_instanciation_statement_list.hpp"

namespace rxc {
	/*!
	 * @brief Collection of all CST node types.
	 *
	 * Every node has two interfaces: One viewing it as a general node and one for viewing its content.
	 *
	 * The one viewing it as a general node contains functions for error reporting, as any node can contain errors.
	 *
	 * When a later stage wants to report an error, it will take the node number as the position. This can then be put into the traverseNode method for getting the lexer position.
	 *
	 * When the next stage wants to access it instead, it uses it by the content interface. However, that content interface contains the part of the next stage that is specific for every node, meaning it doesn't need to be reimplemented. This also allows the actual node to do the error handling, which is important since it is the only one knowing both of its node and content interface (and error reporting is part of the node interface).
	 *
	 * @internal
	 * Since a node may need all of its children for both interfaces (but only their corresponding interface), a node needs to know both interfaces of its child. This currently is implemented by a side cast (using `dynamic_cast`).
	 */
	namespace cst {}
}

#endif
