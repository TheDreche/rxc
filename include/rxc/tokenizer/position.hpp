/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_TOKENIZER_POSITION_HPP
#define RXC_TOKENIZER_POSITION_HPP

/*!
 * @file
 * @brief Lexer position.
 * @sa rxc::tokenizer::Position
 */

#include "../export.hpp"

#include "../sources/position.hpp"
#include "../stage/position.hpp"

namespace rxc::tokenizer {
	class File;
}

namespace rxc::tokenizer {
	/*!
	 * @brief Lexer position.
	 */
	class LIBRXC_EXPORT Position final : public stage::Position {
	private:
		sources::Position position;

	private:
		explicit Position(const sources::Position&);
		friend File;

	public:
		[[nodiscard]] bool operator==(const Position&) const;
		[[nodiscard]] bool operator!=(const Position&) const;
		[[nodiscard]] bool operator<=(const Position&) const;
		[[nodiscard]] bool operator>=(const Position&) const;
		[[nodiscard]] bool operator< (const Position&) const;
		[[nodiscard]] bool operator> (const Position&) const;
	};
}

#endif
