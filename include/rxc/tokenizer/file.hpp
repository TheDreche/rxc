/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_TOKENIZER_FILE_HPP
#define RXC_TOKENIZER_FILE_HPP

#include <memory>
#include <vector>

/*!
 * @file
 * @brief Lexed file.
 * @sa rxc::tokenizer::File
 */

#include "../export.hpp"

#include "../sources/file.hpp"
#include "../sources/position.hpp"

#include "../stage/result.hpp"

#include "position.hpp"
#include "stage_description.hpp"

namespace rxc::tokenizer {
	class Token;

	/*!
	 * @brief Lexed file.
	 */
	class LIBRXC_EXPORT File final : public stage::Result {
	public:
		class Iterator;

	private:
		using TokenWrapper = std::unique_ptr<const Token>;
		using TokenVector = std::vector<TokenWrapper>;

	public:
		std::unique_ptr<const sources::File> read_file;
	private:
		mutable TokenVector tokens;
		friend Position;

	public:
		explicit File(std::unique_ptr<const sources::File>&&);

	public:
		[[nodiscard]] virtual const stage_description::Error* getError(std::size_t) const override;

		[[nodiscard]] Iterator begin() const;
		[[nodiscard]] Iterator end() const;

		void increase(Position&) const;
		[[nodiscard]] const Token* dereference(const Position&) const;

	private:
		TokenWrapper lex(sources::Position&) const;
		void lex_all() const;
	};

	class LIBRXC_EXPORT File::Iterator {
	private:
		TokenVector::const_iterator iter;

	private:
		explicit Iterator(TokenVector::const_iterator&&);
		friend File;

	public:
		[[nodiscard]] Position operator*() const;
		Iterator& operator++();
		[[nodiscard]] bool operator==(const Iterator&) const;
		[[nodiscard]] bool operator!=(const Iterator&) const;
	};
}

#endif
