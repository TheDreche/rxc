/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_TOKENIZER_LITERAL_IDENTIFIER_HPP
#define RXC_TOKENIZER_LITERAL_IDENTIFIER_HPP

#include <memory>
#include <string>

/*!
 * @file
 * @brief Identifier token.
 * @sa rxc::tokenizer::LiteralIdentifier
 */

#include "../export.hpp"

#include "../sources/file.hpp"
#include "../sources/position.hpp"
#include "../sources/span.hpp"

#include "token.hpp"

namespace rxc::tokenizer {
	/*!
	 * @brief Identifer token.
	 */
	class LIBRXC_EXPORT LiteralIdentifier final : public Token {
	private:
		std::string name;

	public:
		explicit LiteralIdentifier(const sources::Span&, std::string&&);

	public:
		[[nodiscard]] static std::unique_ptr<const Token> tokenize(const sources::File&, sources::Position&);
	};
}

#endif
