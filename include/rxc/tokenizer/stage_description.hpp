/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_TOKENIZER_STAGE_DESCRIPTION_HPP
#define RXC_TOKENIZER_STAGE_DESCRIPTION_HPP

/*!
 * @file
 * @brief Tokenizer stage description.
 * @sa rxc::tokenizer::stage_description
 */

#include "../localized_string.hpp"
#include "../error/error_staged.hpp"
#include "../sources/stage_description.hpp"
#include "../stage/description_data.hpp"

#include "position.hpp"

namespace rxc::tokenizer {
	class File;
	class Token;

	namespace stage_description {
		using StageDescription = stage::DescriptionData<
			File,
			Token,
			Position,
			sources::stage_description::StageDescription
		>;
		extern const LocalizedString& stage_name;
		extern const StageDescription stage_description;
		using Error = error::ErrorStaged<StageDescription, &stage_description>;
	}
}

#endif
