/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_TOKENIZER_INVALID_HPP
#define RXC_TOKENIZER_INVALID_HPP

#include <cstddef>

/*!
 * @file
 * @brief Invalid token.
 * @sa rxc::tokenizer::Invalid
 */

#include "../export.hpp"

#include "../error/error_staged.hpp"
#include "../error/invalid_token_start_error.hpp"

#include "../sources/file.hpp"
#include "../sources/span.hpp"

#include "stage_description.hpp"
#include "token.hpp"

namespace rxc::tokenizer {
	/*!
	 * @brief Invalid token.
	 *
	 * This can be used later on to have a place where correction is preferred.
	 */
	class LIBRXC_EXPORT Invalid final : public Token {
	public:
		error::InvalidTokenStartError error;

	public:
		explicit Invalid(const sources::File*, const sources::Span&);

	public:
		[[nodiscard]] virtual const stage_description::Error* traverseError(std::size_t&) const override;
	};
}

#endif
