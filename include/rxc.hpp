/* Copyright (C) 2021-2023 Dreche
 *
 * This file is part of librxc.
 *
 * librxc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * librxc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with librxc.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RXC_HPP
#define RXC_HPP

/*!
 * @file
 * @brief Includes the most commonly used parts of the puplic API of librxc.
 */

/*!
 * @brief Contains all librxc related classes.
 */
namespace rxc {}

#include "rxc/configuration.hpp"
#include "rxc/cst.hpp"
#include "rxc/error.hpp"
#include "rxc/export.hpp"
#include "rxc/initialize.hpp"
#include "rxc/iter.hpp"
#include "rxc/lexer.hpp"
#include "rxc/localized_string.hpp"
#include "rxc/parser.hpp"
#include "rxc/sources.hpp"
#include "rxc/stage.hpp"
#include "rxc/tokenizer.hpp"

#endif
